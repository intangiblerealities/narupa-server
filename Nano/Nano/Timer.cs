﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Diagnostics;

namespace Nano
{
    public static class Timer
    {
        private static readonly long frequency;

        static Timer()
        {
            frequency = Stopwatch.Frequency;
        }

        public static long GetTimestamp()
        {
            return Stopwatch.GetTimestamp();
        }

        public static double TimeSince(long timeStamp)
        {
            return (double)(Stopwatch.GetTimestamp() - timeStamp) / frequency;
        }
    }
}