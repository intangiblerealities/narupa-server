﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Text;

namespace Nano
{
    /// <summary>
    /// Manages the PATH environment variable.
    /// </summary>
    public static class PathManager
    {
        static readonly List<string> paths = new List<string>();
        static readonly string originalPath;

        static PathManager()
        {
            originalPath = Environment.GetEnvironmentVariable("PATH");
        }

        /// <summary>
        /// Add a path to the PATH environment variable.
        /// </summary>
        /// <param name="path">An absolute or '^/' style relative path.</param>
        public static void Add(string path)
        {
            if (paths.Contains(path) == true)
            {
                return; 
            }

            paths.Add(path);

            Update(); 
        }



        /// <summary>
        /// Remove a path from the PATH environment variable.
        /// </summary>
        /// <param name="path">An absolute or '^/' style relative path.</param>
        public static void Remove(string path)
        {
            if (paths.Contains(path) == false)
            {
                return;
            }

            paths.Remove(path);

            Update();
        }

        static void Update()
        {
            StringBuilder sb = new StringBuilder();


            foreach (string path in paths)
            {
                sb.AppendFormat("{0};", Helper.ResolvePath(path)); 
            }

            sb.Append(originalPath); 

            Environment.SetEnvironmentVariable("PATH", sb.ToString());
        }
    }


}
