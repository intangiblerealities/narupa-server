﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace Nano
{
    /// <summary>
    /// Helper methods of all flavors.
    /// </summary>
    /// <autogeneratedoc />
    public static partial class Helper
    {
        private static Regex m_ValidWindowsSerialPortRegex = new Regex(@"^\w+\d+", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        /// <summary>
        /// Struct ComPortInfo
        /// </summary>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for ComPortInfo
        private struct ComPortInfo : IComparable<ComPortInfo>
        {
            /// <summary>
            /// The name
            /// </summary>
            /// <autogeneratedoc />
            /// TODO Edit XML Comment Template for Name
            public string Name;
            /// <summary>
            /// The order
            /// </summary>
            /// <autogeneratedoc />
            /// TODO Edit XML Comment Template for Order
            public int Order;

            /// <summary>
            /// Compares to.
            /// </summary>
            /// <param name="other">The other.</param>
            /// <returns>System.Int32.</returns>
            /// <autogeneratedoc />
            /// TODO Edit XML Comment Template for CompareTo
            public int CompareTo(ComPortInfo other)
            {
                return Order - other.Order; 
            }
        }

        /// <summary>
        /// Gets the serial port names.
        /// </summary>
        /// <returns>System.String[].</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for GetSerialPortNames
        public static string[] GetSerialPortNames()
        {
            string[] ports = SerialPort.GetPortNames();

            List<ComPortInfo> portInfos = new List<ComPortInfo>(ports.Length); 

            for (int i = 0; i < ports.Length; i++)
            {
                ComPortInfo port = CleanUpSerialPortName(ports[i]); 

                if (String.IsNullOrEmpty(port.Name) == true) 
                {
                    continue; 
                }

                portInfos.Add(port); 
            }

            portInfos.Sort();

            string[] finalPorts = new string[portInfos.Count];

            for (int i = 0; i < finalPorts.Length; i++)
            {
                finalPorts[i] = portInfos[i].Name; 
            }

            return finalPorts; 
        }

        private static ComPortInfo CleanUpSerialPortName(string name)
        {
            ComPortInfo result = new ComPortInfo()
            {
                Name = null, Order = 0,
            }; 

            if (name.StartsWith("/") == true)
            {
                if (name.StartsWith("/dev/tty.") == false)
                {
                    return result;
                }

                result.Name = name;
                result.Order = name.GetHashCode(); 

                return result;
            }

            if (m_ValidWindowsSerialPortRegex.IsMatch(name) == true)
            {
                result.Name = m_ValidWindowsSerialPortRegex.Match(name).Value;
                result.Order = int.Parse(result.Name.Substring(3)); 
            }

            return result; 
        }
    }
}
