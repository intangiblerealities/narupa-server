﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using Rug.Osc;
using System.Collections.Generic;
using System.Net;

namespace Nano.Transport.OSCCommands
{
    /// <summary>
    /// Represents a context for managing Osc Commands.
    /// </summary>
    public class OscCommandContext
    {
        ///// <summary>
        ///// The reporter.
        ///// </summary>
        //public readonly IReporter Reporter;

        /// <summary>
        /// The list of commands currently known.
        /// </summary>
        public readonly List<OscCommandAttribute> Commands = new List<OscCommandAttribute>();

        /// <summary>
        /// The transport context.
        /// </summary>
        public readonly Comms.TransportContext TransportContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="OscCommandContext"/> class.
        /// </summary>
        /// <param name="transportContext">The transport context.</param>
        public OscCommandContext(Comms.TransportContext transportContext)
        {
            TransportContext = transportContext;
        }

        /// <summary>
        /// Transmits the command type information for each currently known command.
        /// </summary>
        public void TransmitCommands(IPEndPoint endpoint)
        {
            foreach (OscCommandAttribute attribute in Commands)
            {
                OscMessage message = new OscMessage(attribute.Address, attribute.TypeTagString);
                TransportContext.Transmitter.Transmit((TransportPacketPriority) TransportPacketPriority.Critical, endpoint, new[] {message});
            }
        }
    }
}