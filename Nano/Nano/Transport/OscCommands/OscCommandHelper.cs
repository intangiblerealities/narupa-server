﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Net;
using System.Reflection;
using Nano.Transport.Comms;
using Rug.Osc;

namespace Nano.Transport.OSCCommands
{
    /// <summary>
    /// Utility methods for OSC Commands.
    /// </summary>
    public static class OscCommandHelper
    {
        /// <summary>
        /// Gets the address of method.
        /// </summary>
        /// <param name="provider">The command provider.</param>
        /// <param name="method">The method.</param>
        /// <returns>System.String.</returns>
        public static string GetAddressOfMethod(ICommandProvider provider, MethodInfo method)
        {
            return GetAddressOfMethod(provider.Address, method);
        }

        /// <summary>
        /// Gets the address of method.
        /// </summary>
        /// <param name="providerAddress">The command provider address.</param>
        /// <param name="method">The method.</param>
        /// <returns>System.String.</returns>
        public static string GetAddressOfMethod(string providerAddress, MethodInfo method)
        {
            string methodName = method.Name;
            return GetAddressOfMethod(providerAddress, methodName);
        }

        /// <summary>
        /// Gets the address of method.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="method">The method.</param>
        /// <returns>System.String.</returns>
        public static string GetAddressOfMethod(ICommandProvider provider, MethodBase method)
        {
            return GetAddressOfMethod(provider.Address, method.Name);
        }

        /// <summary>
        /// Gets the address of method.
        /// </summary>
        /// <param name="providerAddress">The provider address.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns>System.String.</returns>
        public static string GetAddressOfMethod(string providerAddress, string methodName)
        {
            string methodCamel = Char.ToLowerInvariant(methodName[0]) + methodName.Substring(1);
            return providerAddress + "/" + methodCamel;
        }

        /// <summary>
        /// Transmits the error.
        /// </summary>
        /// <param name="transportContext">The transport context.</param>
        /// <param name="originalMessage">The original message.</param>
        /// <param name="attribute">The attribute.</param>
        /// <param name="errorMessage">The error message.</param>
        public static void TransmitError(Comms.TransportContext transportContext, OscMessage originalMessage, OscCommandAttribute attribute, string errorMessage)
        {
            OscMessage reply = new OscMessage(originalMessage.Address, errorMessage);
            transportContext.Transmitter.Transmit((TransportPacketPriority) TransportPacketPriority.Critical, originalMessage.Origin, new[] {reply});
            TransmitHelpMessage(transportContext, originalMessage.Origin, originalMessage.Address, attribute);
        }

        /// <summary>
        /// Transmits the help message.
        /// </summary>
        /// <param name="transportContext">The transport context.</param>
        /// <param name="endPoint">The end point.</param>
        /// <param name="address">The address.</param>
        /// <param name="attribute">The attribute.</param>
        public static void TransmitHelpMessage(Comms.TransportContext transportContext, IPEndPoint endPoint, string address, OscCommandAttribute attribute)
        {
            OscMessage help = new OscMessage(address, attribute.ExampleArguments, attribute.Help);
            transportContext.Transmitter.Transmit((TransportPacketPriority) TransportPacketPriority.Critical, endPoint, new[] {help});
        }

        /// <summary>
        /// Broadcasts the type tag string.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="attribute">The attribute.</param>
        public static void BroadcastTypeTagString(Comms.TransportContext context, OscCommandAttribute attribute)
        {
            OscMessage reply = new OscMessage(attribute.Address, attribute.TypeTagString);

            context.Transmitter.Broadcast(TransportPacketPriority.Critical, reply);
        }

        /// <summary>
        /// Sends the type tag string.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="attribute">The attribute.</param>
        public static void SendTypeTagString(Comms.TransportContext context, IPEndPoint endpoint, OscCommandAttribute attribute)
        {
            OscMessage reply = new OscMessage(attribute.Address, attribute.TypeTagString);
            
            context.Transmitter.Transmit((TransportPacketPriority) TransportPacketPriority.Critical, endpoint, new[] {reply});
        }

        /// <summary>
        /// Gets the custom attribute from the method.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="methodBase">The method base.</param>
        /// <returns>T.</returns>
        public static T GetCustomAttribute<T>(MethodBase methodBase)
        {
            object[] attributes = methodBase.GetCustomAttributes(false);
            T attribute = default(T);
            foreach (object obj in attributes)
            {
                if (obj is T)
                {
                    attribute = (T)obj;
                    break;
                }
            }
            return attribute;
        }
    }
}
