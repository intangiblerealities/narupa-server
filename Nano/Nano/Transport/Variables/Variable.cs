﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Rug.Osc;
using SlimMath;

namespace Nano.Transport.Variables
{
    internal abstract class VariableBase : IVariable
    {
        public VariableName Name { get; }

        public bool Readonly { get; }

        public abstract VariableType VariableType { get; }

        public DateTime Timestamp { get; set; }

        public event VariableChangedEvent ValueChanged;
        public event VariableChangedEvent DesiredValueChanged;
        public event VariableChangedEvent Changed;

        protected VariableBase(VariableName name, bool @readonly)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name)); 

            Readonly = @readonly;
        }

        public abstract void FromMessage(OscMessage message);

        public abstract OscMessage ToMessage();

        protected void CheckMessage<T>(OscMessage message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            if (message.Count != 2)
            {
                throw new SimboxException(SimboxExceptionType.UnexpectedNumberOfArguments);
            }

            if ((message[0] is T) == false)
            {
                throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, "Desired value is of the wrong type.");
            }

            if ((message[1] is T) == false)
            {
                throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, "Current value is of the wrong type.");
            }
        }

        protected void OnValueChanged(VariableChangeSource type)
        {
            ValueChanged?.Invoke(this, type);
        }

        protected void OnDesiredValueChanged(VariableChangeSource type)
        {
            DesiredValueChanged?.Invoke(this, type);
        }

        protected void OnChanged(VariableChangeSource type)
        {
            Changed?.Invoke(this, type);
        }             
    }

    internal abstract class VariableBase<T> : VariableBase, IVariable<T> // where T : struct
    {
        private VariableType variableType;
        protected T desiredValue;
        protected T value;

        public override VariableType VariableType { get { return variableType; } }

        public T DesiredValue
        {
            get { return desiredValue; }
            set
            {
                desiredValue = value;
                OnDesiredValueChanged(VariableChangeSource.Local);
                OnChanged(VariableChangeSource.Local);
            }
        }

        public T Value
        {
            get { return value; }
            set
            {
                this.value = value;
                OnValueChanged(VariableChangeSource.Local);
                OnChanged(VariableChangeSource.Local);
            }
        }

        public VariableBase(VariableName name, bool @readonly) : base(name, @readonly)
        {
            variableType = VariableTypes.GetVariableType(typeof(T)); 

            if (variableType == VariableType.Unknown)
            {
                throw new ArgumentException($@"Type ""{typeof(T)}"" is not a valid variable type.");
            }
        }

        public override void FromMessage(OscMessage message)
        {
            CheckMessage<T>(message);

            Timestamp = message.TimeTag?.ToDataTime() ?? DateTime.Now;

            T oldDesiredValue = desiredValue;
            T oldValue = value;

            desiredValue = (T)message[0];
            value = (T)message[1];

            CheckForChanges(oldValue, oldDesiredValue);
        }

        public override OscMessage ToMessage()
        {
            return new OscMessage(Name.Address, DesiredValue, Value);
        }

        protected void CheckForChanges(T oldValue, T oldDesiredValue)
        {
            bool changed = false;

            if (oldValue == null || oldValue.Equals(value) == false)
            {
                OnValueChanged(VariableChangeSource.Remote);
                changed = true;
            }

            if (oldDesiredValue == null || oldDesiredValue.Equals(desiredValue) == false)
            {
                OnDesiredValueChanged(VariableChangeSource.Remote);
                changed = true;
            }

            if (changed == true)
            {
                OnChanged(VariableChangeSource.Local);
            }
        }
    }
    
    internal sealed class Variable_Half : VariableBase<Half>
    {
        public Variable_Half(VariableName name, bool @readonly) : base(name, @readonly) { }

        public override void FromMessage(OscMessage message)
        {            
            CheckMessage<int>(message);

            Timestamp = message.TimeTag?.ToDataTime() ?? DateTime.Now;

            Half oldDesiredValue = desiredValue;
            Half oldValue = value;

            int desired = (int)message[0];
            int current = (int)message[1];

            desiredValue = (Half)desired;
            value = (Half)current;

            CheckForChanges(oldValue, oldDesiredValue);
        }

        public override OscMessage ToMessage()
        {
            return new OscMessage(Name.Address, 
                unchecked((int)DesiredValue.RawValue),
                unchecked((int)Value.RawValue));
        }
    }

    internal sealed class Variable_Single : VariableBase<float>
    {
        public Variable_Single(VariableName name, bool @readonly) : base(name, @readonly) { }
    }

    internal sealed class Variable_Double : VariableBase<double>
    {
        public Variable_Double(VariableName name, bool @readonly) : base(name, @readonly) { }
    }

    internal sealed class Variable_Bool : VariableBase<bool>
    {
        public Variable_Bool(VariableName name, bool @readonly) : base(name, @readonly) { }
    }

    internal sealed class Variable_Int8 : VariableBase<sbyte>
    {
        public Variable_Int8(VariableName name, bool @readonly) : base(name, @readonly) { }

        public override void FromMessage(OscMessage message)
        {
            CheckMessage<byte>(message);

            Timestamp = message.TimeTag?.ToDataTime() ?? DateTime.Now;

            sbyte oldDesiredValue = desiredValue;
            sbyte oldValue = value;

            byte desired = (byte)message[0];
            byte current = (byte)message[1];

            desiredValue = unchecked((sbyte)desired);
            value = unchecked((sbyte)current);

            CheckForChanges(oldValue, oldDesiredValue);
        }

        public override OscMessage ToMessage()
        {
            return new OscMessage(Name.Address,
                unchecked((byte)DesiredValue),
                unchecked((byte)Value));
        }
    }

    internal sealed class Variable_UInt8 : VariableBase<byte>
    {
        public Variable_UInt8(VariableName name, bool @readonly) : base(name, @readonly) { }
    }

    internal sealed class Variable_Int16 : VariableBase<short>
    {
        public Variable_Int16(VariableName name, bool @readonly) : base(name, @readonly) { }

        public override void FromMessage(OscMessage message)
        {
            CheckMessage<int>(message);

            Timestamp = message.TimeTag?.ToDataTime() ?? DateTime.Now;

            short oldDesiredValue = desiredValue;
            short oldValue = value;

            int desired = (int)message[0];
            int current = (int)message[1];

            desiredValue = unchecked((short)desired);
            value = unchecked((short)current);

            CheckForChanges(oldValue, oldDesiredValue);
        }

        public override OscMessage ToMessage()
        {
            return new OscMessage(Name.Address,
                unchecked((int)DesiredValue),
                unchecked((int)Value));
        }
    }

    internal sealed class Variable_UInt16 : VariableBase<ushort>
    {
        public Variable_UInt16(VariableName name, bool @readonly) : base(name, @readonly) { }

        public override void FromMessage(OscMessage message)
        {
            CheckMessage<int>(message);

            Timestamp = message.TimeTag?.ToDataTime() ?? DateTime.Now;

            ushort oldDesiredValue = desiredValue;
            ushort oldValue = value;

            int desired = (int)message[0];
            int current = (int)message[1];

            desiredValue = unchecked((ushort)desired);
            value = unchecked((ushort)current);

            CheckForChanges(oldValue, oldDesiredValue);
        }

        public override OscMessage ToMessage()
        {
            return new OscMessage(Name.Address,
                unchecked((int)DesiredValue),
                unchecked((int)Value));
        }
    }

    internal sealed class Variable_Int32 : VariableBase<int>
    {
        public Variable_Int32(VariableName name, bool @readonly) : base(name, @readonly) { }
    }

    internal sealed class Variable_UInt32 : VariableBase<uint>
    {
        public Variable_UInt32(VariableName name, bool @readonly) : base(name, @readonly) { }

        public override void FromMessage(OscMessage message)
        {
            CheckMessage<int>(message);

            Timestamp = message.TimeTag?.ToDataTime() ?? DateTime.Now;

            uint oldDesiredValue = desiredValue;
            uint oldValue = value;

            int desired = (int)message[0];
            int current = (int)message[1];

            desiredValue = unchecked((uint)desired);
            value = unchecked((uint)current);

            CheckForChanges(oldValue, oldDesiredValue);
        }

        public override OscMessage ToMessage()
        {
            return new OscMessage(Name.Address,
                unchecked((int)DesiredValue),
                unchecked((int)Value));
        }
    }

    internal sealed class Variable_Int64 : VariableBase<long>
    {
        public Variable_Int64(VariableName name, bool @readonly) : base(name, @readonly) { }
    }

    internal sealed class Variable_UInt64 : VariableBase<ulong>
    {
        public Variable_UInt64(VariableName name, bool @readonly) : base(name, @readonly) { }

        public override void FromMessage(OscMessage message)
        {
            CheckMessage<long>(message);

            Timestamp = message.TimeTag?.ToDataTime() ?? DateTime.Now;

            ulong oldDesiredValue = desiredValue;
            ulong oldValue = value;

            long desired = (long)message[0];
            long current = (long)message[1];

            desiredValue = unchecked((ulong)desired);
            value = unchecked((ulong)current);

            CheckForChanges(oldValue, oldDesiredValue);
        }

        public override OscMessage ToMessage()
        {
            return new OscMessage(Name.Address,
                unchecked((long)DesiredValue),
                unchecked((long)Value));
        }
    }

    internal sealed class Variable_String : VariableBase<string>
    {
        public Variable_String(VariableName name, bool @readonly) : base(name, @readonly) { }
    }


    internal abstract class VariableMultiBase<T> : VariableBase<T>
    {
        private int count; 

        public VariableMultiBase(VariableName name, bool @readonly, int count) : base(name, @readonly) { this.count = count; }

        protected new void CheckMessage<V>(OscMessage message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            if (message.Count != count * 2)
            {
                throw new SimboxException(SimboxExceptionType.UnexpectedNumberOfArguments, string.Format("Message had {0} arguments when {1} arguments were expected.", message.Count, count * 2));
            }

            int index = 0; 

            for (int i = 0; i < count; i++)
            {
                if ((message[index++] is V) == false)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, "Desired value is of the wrong type.");
                }
            }

            for (int i = 0; i < count; i++)
            {
                if ((message[index++] is V) == false)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, "Current value is of the wrong type.");
                }
            }
        }
    }

    internal sealed class Variable_Vector2 : VariableMultiBase<Vector2>
    {
        public Variable_Vector2(VariableName name, bool @readonly) : base(name, @readonly, 2) { }

        public override void FromMessage(OscMessage message)
        {
            CheckMessage<float>(message);

            Timestamp = message.TimeTag?.ToDataTime() ?? DateTime.Now;

            Vector2 oldDesiredValue = desiredValue;
            Vector2 oldValue = value;

            int index = 0;
            Vector2 desired = new Vector2((float)message[index++], (float)message[index++]);
            Vector2 current = new Vector2((float)message[index++], (float)message[index++]);

            desiredValue = desired;
            value = current;

            CheckForChanges(oldValue, oldDesiredValue);
        }

        public override OscMessage ToMessage()
        {
            return new OscMessage(
                Name.Address,
                DesiredValue.X, DesiredValue.Y,
                Value.X, Value.Y
                );
        }
    }


    internal sealed class Variable_Vector3 : VariableMultiBase<Vector3>
    {
        public Variable_Vector3(VariableName name, bool @readonly) : base(name, @readonly, 3) { }

        public override void FromMessage(OscMessage message)
        {
            CheckMessage<float>(message);

            Timestamp = message.TimeTag?.ToDataTime() ?? DateTime.Now;

            Vector3 oldDesiredValue = desiredValue;
            Vector3 oldValue = value;

            int index = 0;
            Vector3 desired = new Vector3((float)message[index++], (float)message[index++], (float)message[index++]);
            Vector3 current = new Vector3((float)message[index++], (float)message[index++], (float)message[index++]);

            desiredValue = desired;
            value = current;

            CheckForChanges(oldValue, oldDesiredValue);
        }

        public override OscMessage ToMessage()
        {
            return new OscMessage(
                Name.Address,
                DesiredValue.X, DesiredValue.Y, DesiredValue.Z,
                Value.X, Value.Y, Value.Z
                );
        }
    }

    internal sealed class Variable_BoundingBox : VariableMultiBase<BoundingBox>
    {
        public Variable_BoundingBox(VariableName name, bool @readonly) : base(name, @readonly, 6) { }

        public override void FromMessage(OscMessage message)
        {
            CheckMessage<float>(message);

            Timestamp = message.TimeTag?.ToDataTime() ?? DateTime.Now;

            BoundingBox oldDesiredValue = desiredValue;
            BoundingBox oldValue = value;

            int index = 0;
            BoundingBox desired = new BoundingBox((float)message[index++], (float)message[index++], (float)message[index++], (float)message[index++], (float)message[index++], (float)message[index++]);
            BoundingBox current = new BoundingBox((float)message[index++], (float)message[index++], (float)message[index++], (float)message[index++], (float)message[index++], (float)message[index++]);

            desiredValue = desired;
            value = current;

            CheckForChanges(oldValue, oldDesiredValue);
        }

        public override OscMessage ToMessage()
        {
            return new OscMessage(
                Name.Address,
                DesiredValue.Minimum.X, DesiredValue.Minimum.Y, DesiredValue.Minimum.Z, DesiredValue.Maximum.X, DesiredValue.Maximum.Y, DesiredValue.Maximum.Z,
                Value.Minimum.X, Value.Minimum.Y, Value.Minimum.Z, Value.Maximum.X, Value.Maximum.Y, Value.Maximum.Z
                );
        }
    }
}
