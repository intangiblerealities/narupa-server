﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.Variables
{
    public static class VariableFactory
    {
        /// <summary>
        /// Create a <see cref="IVariable"/> instance for the supplied <see cref="VariableType"/>.
        /// </summary>
        /// <param name="name">A name for the variable.</param>
        /// <param name="type">The type of the variable.</param>
        /// <param name="isReadonly">If true the variable is marked as read-only.</param>
        /// <returns>A IVariable instance.</returns>
        public static IVariable Create(VariableName name, VariableType type, bool isReadonly)
        {
            switch (type)
            {
                case VariableType.Half:
                    return new Variable_Half(name, isReadonly);                    
                case VariableType.Float:
                    return new Variable_Single(name, isReadonly);
                case VariableType.Double:
                    return new Variable_Double(name, isReadonly);
                case VariableType.Bool:
                    return new Variable_Bool(name, isReadonly);
                case VariableType.Int8:
                    return new Variable_Int8(name, isReadonly);
                case VariableType.UInt8:
                    return new Variable_UInt8(name, isReadonly);
                case VariableType.Int16:
                    return new Variable_Int16(name, isReadonly);
                case VariableType.UInt16:
                    return new Variable_UInt16(name, isReadonly);
                case VariableType.Int32:
                    return new Variable_Int32(name, isReadonly);
                case VariableType.UInt32:
                    return new Variable_UInt32(name, isReadonly);
                case VariableType.Int64:
                    return new Variable_Int64(name, isReadonly);
                case VariableType.UInt64:
                    return new Variable_UInt64(name, isReadonly);
                case VariableType.String:
                    return new Variable_String(name, isReadonly);

                case VariableType.Vector2:
                    return new Variable_Vector2(name, isReadonly);
                case VariableType.Vector3:
                    return new Variable_Vector3(name, isReadonly);
                case VariableType.BoundingBox:
                    return new Variable_BoundingBox(name, isReadonly);

                default:
                    throw new SimboxException(SimboxExceptionType.InvalidVariableType, $@"Variable ""{name}"" could not be created. Unsuported type ""{type}"".");
            }
        }
    }
}
