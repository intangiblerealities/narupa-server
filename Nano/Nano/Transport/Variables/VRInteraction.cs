﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Runtime.InteropServices;

namespace Nano.Transport.Variables
{
    public enum VRDevice { Headset, LeftController, RightController }

    [StructLayout(LayoutKind.Sequential)]
    public struct VRInteraction
    {
        /// <summary>
        /// The interaction.
        /// </summary>
        public Interaction.Interaction Interaction;

        /// <summary>
        /// Quaternion of the tracked object.
        /// </summary>
        public SlimMath.Quaternion Quaternion;

        /// <summary>
        /// Type of device (using <see cref="VRDevice"/>).
        /// </summary>
        public byte DeviceType;
    }
}