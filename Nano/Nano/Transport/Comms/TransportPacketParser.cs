﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using System.Net;
using Nano.Transport.Agents;
using Rug.Osc;

namespace Nano.Transport.Comms
{
    /// <summary>
    /// Handles the parsing of packets from a stream of bytes.
    /// </summary>
    public sealed class TransportPacketParser : IDisposable
    {
        private bool hasHeader = false;
        private int packetSize = 0;
        private TransportPacketType packetType = TransportPacketType.OscBundle;

        private readonly MemoryStream buffer = new MemoryStream(new byte[MaxBufferSize], 0, MaxBufferSize, true, true);

        private ushort packetID = 0;

        /// <summary>
        /// Gets the size of the packet header.
        /// </summary>
        public const int HeaderSize = 6;

        /// <summary>
        /// Gets the maximum buffer size.
        /// </summary>
        public const int MaxBufferSize = 1048576 * 10; // Allocate 10 MB
        //public const int MaxBufferSize = 1024 * 32 * 4 * 2; 

        /// <summary>
        /// Gets the packet receiver that will process completed packets.
        /// </summary>
        public readonly ITransportPacketReceiver Receiver;

        private PacketStatistics packetStatistics;

        /// <summary>
        /// Creates a new packet parser instance.
        /// </summary>
        /// <param name="receiver">Packet receiver that will process completed packets.</param>
        /// <exception cref="System.ArgumentNullException">receiver</exception>
        public TransportPacketParser(ITransportPacketReceiver receiver, PacketStatistics packetStatistics)
        {
            Receiver = receiver ?? throw new ArgumentNullException(nameof(receiver));

            this.packetStatistics = packetStatistics ?? throw new ArgumentNullException(nameof(packetStatistics));

            buffer.SetLength(0);
        }

        /// <summary>
        /// Process a chunk of bytes.
        /// </summary>
        /// <param name="origin">Origin IP end point.</param>
        /// <param name="data">A array of bytes.</param>
        /// <param name="index">Index in the array where reading should begin.</param>
        /// <param name="length">Length of the chunk.</param>
        public void Process(IPEndPoint origin, byte[] data, int index, int length)
        {
            int i = index;
            int remaining = length;             

            while (remaining > 0)
            {                
                ReadChunk(origin, data, ref i, ref remaining); 
            }
        }

        public bool ProcessSinglePacket(IPEndPoint origin, Stream stream)
        {        
            try
            {
                if (stream.Length - stream.Position < HeaderSize)
                {
                    return false;
                }

                stream.Read(buffer.GetBuffer(), (int)buffer.Position, HeaderSize);

                buffer.Position += HeaderSize;

                if (TryParseHeader() == false)
                {
                    return false;
                }

                if (stream.Length - stream.Position < packetSize)
                {
                    return false;
                }

                stream.Read(buffer.GetBuffer(), (int)buffer.Position, packetSize);

                buffer.Position += packetSize; 

                ParseCompletedPacket(origin);

                return true; 
            }
            finally
            {
                //stream.Position = 
            }
        }

        private void ReadChunk(IPEndPoint origin, byte[] data, ref int index, ref int remaining)
        {
            if (hasHeader == false)
            {
                // header is incomplete (needs more bytes)
                if (buffer.Position < HeaderSize)
                {
                    // try and read the remainder of the header
                    int toRead = Math.Min(remaining, HeaderSize - (int)buffer.Position);

                    if (toRead + buffer.Position >= MaxBufferSize)
                    {
                        throw new SimboxException(SimboxExceptionType.BufferSizeMismatch, "Packet size is greater than the maximum packet size.");
                    }

                    buffer.Write(data, index, toRead);

                    index += toRead;
                    remaining -= toRead;
                }

                TryParseHeader();
            }
            else
            {
                // data is incomplete (needs more bytes)
                if (buffer.Position < packetSize + HeaderSize)
                {
                    // try and read the remainder of the of the data
                    int toRead = Math.Min(remaining, packetSize - ((int)buffer.Position - HeaderSize));

                    if (toRead + buffer.Position >= MaxBufferSize)
                    {
                        throw new SimboxException(SimboxExceptionType.BufferSizeMismatch, "Packet size is greater than the maximum packet size."); 
                    }

                    buffer.Write(data, index, toRead);

                    index += toRead;
                    remaining -= toRead;
                }

                // data is complete 
                if (buffer.Position == packetSize + HeaderSize)
                {
                    ParseCompletedPacket(origin);
                }
            }
        }

        private void ParseCompletedPacket(IPEndPoint origin)
        {
            try
            {
                // data is a OSC packet
                if (packetType == TransportPacketType.OscBundle)
                {
                    packetStatistics.IncrementReceiveOscPackets((int)buffer.Position - HeaderSize);

                    // strip out the OSC packet
                    OscBundle bundle = OscBundle.Read(buffer.GetBuffer(), HeaderSize, (int)buffer.Position - HeaderSize, origin);

                    Receiver.OnOscPacket(origin, bundle);
                }
                // data is a data packet
                else
                {
                    packetStatistics.IncrementReceiveStreamPackets((int)buffer.Position);

                    // pass the entire buffer, the header includes the stream information
                    Receiver.OnDataPacket(origin, packetID, buffer.GetBuffer(), 0, (int)buffer.Position);
                }
            }
            catch (Exception ex)
            {
                throw new SimboxException(SimboxExceptionType.UnhandledExceptionWhileProcessingPacket, "There was an exception while processing a packet.", ex);
            }
            finally
            {
                // reset the puffer for the next packet
                buffer.Position = 0;
                packetType = TransportPacketType.OscBundle;
                hasHeader = false;
            }
        }

        private bool TryParseHeader()
        {
            if (buffer.Position != HeaderSize)
            {
                return false;
            }

            // header is complete
            byte[] header = buffer.GetBuffer();

            // read the packet id
            packetID = BitConverter.ToUInt16(header, 0);

            // assign packet type OSC or Data
            packetType = (TransportPacketType)Math.Min((int)packetID, 1);

            // read the length of the packet in bytes
            packetSize = BitConverter.ToInt32(header, 2);

            if (packetType == TransportPacketType.OscBundle &&
                packetSize == 0)
            {
                throw new SimboxException(SimboxExceptionType.InvalidPacketSize, "OSC Packets must be greater than 0 bytes in length.");
            }

            hasHeader = true;

            return true; 
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting resources.
        /// </summary>
        /// <autogeneratedoc />
        public void Dispose()
        {
            buffer.Dispose();
        }
    }
}
