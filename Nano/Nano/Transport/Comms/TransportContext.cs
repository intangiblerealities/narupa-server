﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Agents;
using Rug.Osc;

namespace Nano.Transport.Comms
{
    /// <summary>
    /// Represents a transport context used to contain commonly used transport instances.
    /// </summary>
    public class TransportContext
    {
        /// <summary>
        /// Gets the osc manager.
        /// </summary>
        /// <value>The osc manager.</value>
        public OscAddressManager OscManager { get; }

        /// <summary>
        /// Gets the reporter.
        /// </summary>
        /// <value>The reporter.</value>
        public IReporter Reporter { get; }

        /// <summary>
        /// Gets the statistics.
        /// </summary>
        /// <value>The statistics.</value>
        public PacketStatistics Statistics { get; }

        /// <summary>
        /// Gets the transmitter.
        /// </summary>
        /// <value>The transmitter.</value>
        public ITransportPacketTransmitter Transmitter { get; internal set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportContext"/> class.
        /// </summary>
        /// <param name="oscManager">The osc manager.</param>
        /// <param name="transmitter">The transmitter.</param>
        /// <param name="statistics">The statistics.</param>
        /// <param name="reporter"></param>
        public TransportContext(OscAddressManager oscManager, ITransportPacketTransmitter transmitter, PacketStatistics statistics, IReporter reporter)
        {
            OscManager = oscManager;
            Transmitter = transmitter;
            Statistics = statistics;
            Reporter = reporter;
        }
    }
}