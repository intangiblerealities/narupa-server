﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Rug.Osc;

namespace Nano.Transport.Comms
{
    /// <summary>
    /// Client / server communication commands.
    /// </summary>
    public static partial class Commands
    {
        /// <summary>
        /// Holder for all the messages for the leave stream command.
        /// </summary>
        public static class LeaveStream
        {
            /// <summary>
            /// Create a OSC message for a leave stream request command.
            /// </summary>
            /// <param name="id">ID of the stream.</param>
            /// <returns>A command OSC message.</returns>
            public static OscMessage Request(ushort id)
            {
                return StreamCommand(GetAddress(Command.LeaveStream), id);
            }

            /// <summary>
            /// Create a OSC message for a leave stream success response command.
            /// </summary>
            /// <param name="id">ID of the stream.</param>
            /// <returns>A command OSC message.</returns>
            public static OscMessage Success(ushort id)
            {
                return StreamCommand(GetAddress(Command.LeaveStream), id, CommandState.Success);
            }

            /// <summary>
            /// Create a OSC message for a leave stream failed response command.
            /// </summary>
            /// <param name="id">ID of the stream.</param>
            /// <param name="error">Error string.</param>
            /// <returns>A command OSC message.</returns>
            public static OscMessage Failed(ushort id, string error)
            {
                return StreamCommand(GetAddress(Command.LeaveStream), id, CommandState.Failed, error);
            }

            /// <summary>
            /// Parse a leave stream command OSC message.
            /// </summary>
            /// <param name="message">OSC message to parse.</param>
            /// <param name="id">Outputs the ID of the stream.</param>
            /// <param name="state">Outputs the state of the command.</param>
            /// <param name="error">Outputs the error string of the command if it failed.</param>
            public static void Parse(OscMessage message, out ushort id, out CommandState state, out string error)
            {
                ParseStreamCommand(message, out id, out state, out error);
            }
        }
    }
}
