﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Rug.Osc;

namespace Nano.Transport.Comms
{
    /// <summary>
    /// Simbox commands.
    /// </summary>
    public enum Command
    {
        /// <summary>
        /// Check the version of the API for compatibility.
        /// </summary>
        ApiVersion,

        /// <summary>
        /// List all the streams.
        /// </summary>
        ListStreams,

        /// <summary>
        /// Gets or sets the format for a data stream. 
        /// </summary>
        StreamFormat,

        /// <summary>
        /// Start receiving data from a stream. 
        /// </summary>
        JoinStream,

        /// <summary>
        /// Stop receiving data from a stream. 
        /// </summary>
        LeaveStream,

        /// <summary>
        /// Destroy a data stream. 
        /// </summary>
        DestroyStream,

        /// <summary>
        /// List all variables.
        /// </summary>
        ListVariables
    }

    /// <summary>
    /// The state of a command. 
    /// </summary>
    public enum CommandState : byte
    {
        /// <summary>
        /// The command is being / has been sent and wants a response.
        /// </summary>
        Request = 0,

        /// <summary>
        /// The command was successful.
        /// </summary>
        Success = 1,

        /// <summary>
        /// The command has failed.
        /// </summary>
        Failed = 2,

        /// <summary>
        /// Header of a list.
        /// </summary>
        ListHeader = 3,

        /// <summary>
        /// An item in a list.
        /// </summary>
        ListItem = 4
    }

    /// <summary>
    /// Client / server communication commands.
    /// </summary>
    public static partial class Commands
    {
        /// <summary>
        /// The command state mask.
        /// </summary>
        /// <autogeneratedoc />
        public const int CommandStateMask = 0x0000F000;

        /// <summary>
        /// The address for the API version command.
        /// </summary>
        /// <autogeneratedoc />
        internal const string Address_ApiVersion = "/api/version";

        /// <summary>
        /// The address for the destroy stream command.
        /// </summary>
        /// <autogeneratedoc />
        internal const string Address_DestroyStream = "/stream/destroy";

        /// <summary>
        /// The address for the join stream command.
        /// </summary>
        /// <autogeneratedoc />
        internal const string Address_JoinStream = "/stream/join";

        /// <summary>
        /// The address for the leave stream command.
        /// </summary>
        /// <autogeneratedoc />
        internal const string Address_LeaveStream = "/stream/leave";

        /// <summary>
        /// The address for the list streams command.
        /// </summary>
        /// <autogeneratedoc />
        internal const string Address_ListStreams = "/stream/list";

        /// <summary>
        /// The address for the list variables command.
        /// </summary>
        /// <autogeneratedoc />
        internal const string Address_ListVariables = "/variable/list";

        /// <summary>
        /// The address for the set stream format command.
        /// </summary>
        /// <autogeneratedoc />
        internal const string Address_SetStreamFormat = "/stream/format";

        /// <summary>
        /// Get the OSC address for a command type.
        /// </summary>
        /// <param name="command">A command type.</param>
        /// <returns>The OSC address associated with the command.</returns>
        /// <exception cref="SimboxException"></exception>
        public static string GetAddress(Command command)
        {
            switch (command)
            {
                case Command.ApiVersion:
                    return Address_ApiVersion;
                case Command.ListStreams:
                    return Address_ListStreams;
                case Command.StreamFormat:
                    return Address_SetStreamFormat;
                case Command.JoinStream:
                    return Address_JoinStream;
                case Command.LeaveStream:
                    return Address_LeaveStream;
                case Command.DestroyStream:
                    return Address_DestroyStream;
                case Command.ListVariables:
                    return Address_ListVariables;
                default:
                    throw new SimboxException(SimboxExceptionType.UnknownCommand, Strings.Error_UnknownCommand);
            }
        }

        /// <summary>
        /// Gets the command identifier from a packet header.
        /// </summary>
        /// <param name="packetValue">The packet value.</param>
        /// <returns>System.UInt16.</returns>
        /// <autogeneratedoc />
        public static ushort GetCommandId(uint packetValue)
        {
            return unchecked((ushort)((packetValue & 0xFFFF0000) >> 16));
        }

        /// <summary>
        /// Gets the command identifier from a packet header.
        /// </summary>
        /// <param name="packetValue">The packet value.</param>
        /// <returns>System.UInt16.</returns>
        /// <autogeneratedoc />
        public static ushort GetCommandId(int packetValue)
        {
            return unchecked((ushort)((packetValue & 0xFFFF0000) >> 16));
        }

        /// <summary>
        /// Gets the state of the command from a packet header.
        /// </summary>
        /// <param name="packetValue">The packet value.</param>
        /// <returns>CommandState.</returns>
        /// <autogeneratedoc />
        public static CommandState GetCommandState(uint packetValue)
        {
            return (CommandState)((packetValue & CommandStateMask) >> 12);
        }

        /// <summary>
        /// Gets the state of the command from a packet header.
        /// </summary>
        /// <param name="packetValue">The packet value.</param>
        /// <returns>CommandState.</returns>
        /// <autogeneratedoc />
        public static CommandState GetCommandState(int packetValue)
        {
            return (CommandState)((packetValue & CommandStateMask) >> 12);
        }

        private static int PackIdAndState(ushort id, CommandState state)
        {
            return unchecked((int)(uint)((id << 16) | (((byte)state & 0x0F) << 12)));
        }

        /// <summary>
        /// Parse a command that relates to a stream.
        /// </summary>
        /// <param name="message">OSC message to parse from.</param>
        /// <param name="id">The id of the stream.</param>
        /// <param name="state">The state of the command.</param>
        /// <param name="error">The error message or string.Empty if there is none.</param>
        private static void ParseStreamCommand(OscMessage message, out ushort id, out CommandState state, out string error)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            if (message.Count < 1)
            {
                throw new SimboxException(SimboxExceptionType.UnexpectedNumberOfArguments);
            }

            if ((message[0] is int) == false)
            {
                throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, string.Format(Strings.Error_Argument_WrongType, 0));
            }

            int value = (int)message[0];

            UnpackIdAndState(value, out id, out state);

            error = string.Empty;

            if (state == CommandState.Failed && message.Count >= 2)
            {
                if ((message[1] is string) == false)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, string.Format(Strings.Error_Argument_WrongType, 1));
                }

                error = (string)message[1];
            }
        }

        /// <summary>
        /// Create stream command.
        /// </summary>
        /// <param name="address">OSC address.</param>
        /// <param name="id">The id of the stream.</param>
        /// <param name="state">The state of the command.</param>
        /// <param name="error">An error message.</param>
        /// <returns>A OSC message.</returns>
        private static OscMessage StreamCommand(string address, ushort id, CommandState state = CommandState.Request, string error = null)
        {
            int packedValue = PackIdAndState(id, state);

            if (state == CommandState.Failed)
            {
                if (string.IsNullOrEmpty(error))
                {
                    throw new ArgumentNullException("error", Strings.Command_Failed_MissingMessage);
                }

                return new OscMessage(address, packedValue, error) { TimeTag = OscTimeTag.Now };
            }
            return new OscMessage(address, packedValue) { TimeTag = OscTimeTag.Now };
        }

        private static void UnpackIdAndState(int value, out ushort id, out CommandState state)
        {
            id = GetCommandId(value);
            state = GetCommandState(value);
        }
    }
}
