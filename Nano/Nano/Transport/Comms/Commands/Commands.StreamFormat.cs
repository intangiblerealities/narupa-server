﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using Rug.Osc;

namespace Nano.Transport.Comms
{
    /// <summary>
    /// Client / server communication commands.
    /// </summary>
    public static partial class Commands
    {
        /// <summary>
        /// Holder for all the messages for the stream format commands.
        /// </summary>
        public static class StreamFormat
        {
            /// <summary>
            /// Create a OSC message for a get stream format message.
            /// </summary>
            /// <param name="id">ID of the stream.</param>
            /// <returns>A command OSC message.</returns>
            public static OscMessage Get(ushort id)
            {
                return StreamCommand(GetAddress(Command.StreamFormat), id, CommandState.Request, null);
            }

            /// <summary>
            /// Create a OSC message for a set stream format command.
            /// </summary>
            /// <param name="id">ID of the stream.</param>
            /// <param name="type">Variable type.</param>
            /// <param name="channelCount">Number of channels (X,Y,Z,W etc).</param>
            /// <param name="maxCount">Maximum</param>
            /// <returns>A command OSC message.</returns>
            public static OscMessage Set(ushort id, VariableType type, int channelCount, int maxCount)
            {
                return new TransportStreamFormatDescriptor(id, type, channelCount, maxCount).ToMessage(CommandState.Success, null);
            }

            /// <summary>
            /// Create a OSC message for a set stream format command.
            /// </summary>
            /// <param name="descriptor">Stream format descriptor.</param>
            /// <returns>A command OSC message.</returns>
            public static OscMessage Set(ref TransportStreamFormatDescriptor descriptor)
            {
                return descriptor.ToMessage(CommandState.Success, null);
            }

            /// <summary>
            /// Create a OSC message for a stream format failed response command.
            /// </summary>
            /// <param name="id">ID of the stream.</param>
            /// <param name="error">Error string.</param>
            /// <returns>A command OSC message.</returns>
            public static OscMessage Failed(ushort id, string error)
            {
                return StreamCommand(GetAddress(Command.StreamFormat), id, CommandState.Failed, error);
            }

            /// <summary>
            /// Parse a stream format command OSC message.
            /// </summary>
            /// <param name="message">OSC message to parse.</param>
            /// <param name="id">Outputs the ID of the stream.</param>
            /// <param name="state">Outputs the state of the command.</param>
            /// <param name="error">Outputs the error string of the command if it failed.</param>
            public static void Parse(OscMessage message, out ushort id, out CommandState state, out string error)
            {
                ParseStreamCommand(message, out id, out state, out error);
            }

            /// <summary>
            /// Parse a set stream format command from a OSC message.
            /// </summary>
            /// <param name="message">OSC message to parse.</param>
            /// <param name="descriptor">Will output the stream format descriptor of the item.</param>
            /// <param name="state">Command state.</param>
            /// <param name="error">Error string.</param>
            public static void ParseDescriptor(OscMessage message, out TransportStreamFormatDescriptor descriptor, out CommandState state, out string error)
            {
                descriptor = TransportStreamFormatDescriptor.FromMessage(message, out state, out error);
            }

            /// <summary>
            /// Determine if a OSC message is a get stream format request message.
            /// </summary>
            /// <param name="message">A OSC message.</param>
            /// <returns>True if the message is a get stream format request message.</returns>
            public static bool IsGet(OscMessage message)
            {
                ushort id;
                CommandState state;
                string error;

                Parse(message, out id, out state, out error);

                switch (state)
                {
                    case CommandState.Request:
                        return message.Count == 1;
                    default:
                        return false;
                }
            }
        }
    }
}
