﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Net;
using System.Net.Sockets;

namespace Nano.Transport.Comms
{
    public class ConnectionString
    {
        public ConnectionType ConnectionType { get; }
        public IPAddress IPAddress { get; }
        public int Port { get; }
        public Uri Uri { get; }

        public ConnectionString(IPAddress address, int port)
        {
            ConnectionType = ConnectionType.Tcp;
            Uri = new Uri($"net.tcp://{address}:{port}/");
            IPAddress = address;
            Port = port;
        }

        public ConnectionString(Uri uri)
        {
            Uri = uri;

            if (TryResolveAddressAndPort(Uri, out IPAddress address, out int port) == false)
            {
                throw new ArgumentException($@"Could not resolve a host for uri {Uri}", nameof(uri));
            }

            IPAddress = address;
            Port = port;

            // "net.pipe"
            ConnectionType = Uri.Scheme == "net.tcp" ? ConnectionType.Tcp : ConnectionType.WebSocket;
        }

        public override bool Equals(object obj)
        {
            switch (obj)
            {
                case string _:
                    return ToString().Equals((string)obj);

                case Uri _:
                    return Uri.Equals((Uri)obj);

                case ConnectionString _:
                    return Uri.Equals(((ConnectionString)obj).Uri);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Uri.GetHashCode();
        }

        public override string ToString()
        {
            return Uri.ToString();
        }

        private static bool TryResolveAddressAndPort(Uri uri, out IPAddress address, out int port)
        {
            port = uri.Port;

            if (IPAddress.TryParse(uri.DnsSafeHost, out address) == true)
            {
                return true;
            }

            IPHostEntry hostEntry = Dns.GetHostEntry(uri.DnsSafeHost);

            foreach (IPAddress var in hostEntry.AddressList)
            {
                if (var.AddressFamily != AddressFamily.InterNetwork)
                {
                    continue;
                }

                address = var;

                return true;
            }

            address = null;

            return false;
        }
    }
}