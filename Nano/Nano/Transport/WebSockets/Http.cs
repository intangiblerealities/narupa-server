﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Net;

namespace Nano.WebSockets
{
    public delegate void HttpSendEvent(Http http, byte[] buffer, int index, int count);

    public class Http
    {
        public const int BadRequest = 400;
        public const int ChangeProtocol = 101;

        public readonly string EndPoint;
        //public readonly Stream Stream;

        private IReporter reporter;

        public event HttpSendEvent HttpComplete;

        public Http(string endPoint, IReporter reporter)
        {
            EndPoint = endPoint;
            this.reporter = reporter;
        }

        public bool ProcessClientHandshake(byte[] buffer, int index, int length, string requiredClientKey = null)
        {
            Header header = new Header(buffer, index, length);

            if (header.Method != HttpMethod.GET)
            {
                reporter.PrintError($@"Unexpected HTTP method ""{header.Method}""");
                Respond(HttpStatusCode.BadRequest);
                return false;
            }

            if (header.Version != "HTTP/1.1")
            {
                reporter.PrintError($@"Unexpected HTTP version code ""{header.Version}""");
                Respond(HttpStatusCode.BadRequest);
                return false;
            }

            if (header.CheckValue("Upgrade", "WebSocket", true) == false)
            {
                reporter.PrintError($@"Missing or invalid ""Upgrade"" token");
                Respond(HttpStatusCode.BadRequest);
                return false;
            }

            if (header.CheckValue("Connection", "Upgrade", true) == false)
            {
                reporter.PrintError($@"Missing or invalid ""Connection"" token");
                Respond(HttpStatusCode.BadRequest);
                return false;
            }

            string hostName;
            if (header.TryGetValue("Host", out hostName) == false)
            {
                reporter.PrintError($@"Missing ""Host"" token");
                Respond(HttpStatusCode.BadRequest, "Missing host");
                return false;
            }

            if (header.Uri.ToString() != "/")
            {
                reporter.PrintError($@"Invalid path");
                Respond(HttpStatusCode.BadRequest, "Unknown path");
                return false;
            }

            string clientKey;
            if (header.TryGetValue("Sec-WebSocket-Key", out clientKey) == false)
            {
                reporter.PrintError($@"Missing ""Sec-WebSocket-Key"" token");
                Respond(HttpStatusCode.BadRequest, "Missing security key");
                return false;
            }

            if (header.CheckValue("Sec-WebSocket-Version", "13", true) == false)
            {
                reporter.PrintError($@"Missing or invalid ""Sec-WebSocket-Version"" token");
                Respond(HttpStatusCode.BadRequest, "Unsupported websocket version");
                return false;
            }

            if (requiredClientKey != null && clientKey != requiredClientKey)
            {
                reporter.PrintError($@"Invalid client session key ""{clientKey}""");
                Respond(HttpStatusCode.Unauthorized, "Invalid client key");
                return false;
            }

            reporter.PrintEmphasized("Handshake accepted.");

            Header response = new Header(HttpStatusCode.SwitchingProtocols, "Web Socket Protocol Handshake");

            response.Add("Upgrade", "webSocket");
            response.Add("Connection", "Upgrade");
            response.Add("cache-control", "no-cache");
            response.Add("Sec-WebSocket-Accept", Keys.GetAcceptString(clientKey));

            RespondWithHeader(response);

            return true;
        }

        public bool ProcessServerHandshake(byte[] buffer, int index, int length, string clientKey)
        {
            Header header = new Header(buffer, index, length);

            if (header.Method != HttpMethod.StatusCode)
            {
                reporter.PrintError($@"Unexpected HTTP method ""{header.Method}""");
                return false;
            }

            if (header.ResponseCode != HttpStatusCode.SwitchingProtocols)
            {
                reporter.PrintError($@"Unexpected response code ""{header.ResponseCode}""");
                return false;
            }

            if (header.Version != "HTTP/1.1")
            {
                reporter.PrintError($@"Unexpected HTTP version code ""{header.Version}""");
                return false;
            }

            if (header.CheckValue("Upgrade", "WebSocket", true) == false)
            {
                reporter.PrintError($@"Missing or invalid ""Upgrade"" token");
                return false;
            }

            if (header.CheckValue("Connection", "Upgrade", true) == false)
            {
                reporter.PrintError($@"Missing or invalid ""Connection"" token");
                return false;
            }

            string acceptKey;
            if (header.TryGetValue("Sec-WebSocket-Accept", out acceptKey) == false)
            {
                reporter.PrintError($@"Missing ""Sec-WebSocket-Accept"" token");
                return false; 
            }

            if (acceptKey.Equals(Keys.GetAcceptString(clientKey)) == false)
            {
                reporter.PrintError($@"Invalid security token ""{acceptKey}""");
                return false;
            }

            reporter.PrintEmphasized("Handshake accepted.");

            return true;
        }

        public void Respond(HttpStatusCode code, string message = null)
        {
            reporter.PrintWarning(ReportVerbosity.Normal, $@"Respond: {code}, {message}");

            Header response = new Header(code, message);

            RespondWithHeader(response);
        }

        public void RespondWithHeader(Header response)
        {
            byte[] responseBuffer = response.ToBytes();

            HttpComplete?.Invoke(this, responseBuffer, 0, responseBuffer.Length);

            reporter.PrintNormal(Direction.Transmit, EndPoint, $@"Sent HTTP packet ({responseBuffer.Length} bytes)");
        }

        public void SendClientHandshake(string host, string path, string clientKey)
        {
            reporter.PrintNormal(Direction.Transmit, EndPoint, $@"Send web-socket handshake");

            Header request = new Header(HttpMethod.GET, path);

            request.Add("Host", host);
            request.Add("Upgrade", "WebSocket");
            request.Add("Connection", "Upgrade");
            request.Add("cache-control", "no-cache");
            request.Add("Sec-WebSocket-Key", clientKey);
            request.Add("Sec-WebSocket-Version", "13");

            RespondWithHeader(request);
        }
    }
}