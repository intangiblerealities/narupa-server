﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Nano.WebSockets
{
    public enum HttpMethod
    {
        StatusCode,

        CONNECT,
        DELETE,
        GET,
        HEAD,
        OPTIONS,
        POST,
        PUT,
    }

    public class Header
    {
        private readonly Dictionary<string, string> header = new Dictionary<string, string>();

        public string Message { get; private set; }
        public HttpMethod Method { get; private set; }

        public Uri Uri { get; private set; }

        public HttpStatusCode ResponseCode { get; private set; } = 0;
        public string Version { get; private set; }

        public int HeaderLength { get; } 

        public string this[string name]
        {
            get => header[name.ToLowerInvariant()];
            set => header[name.ToLowerInvariant()] = value;
        }

        public Header(byte[] buffer, int index, int count)
        {
            HeaderLength = 0; 

            using (MemoryStream stream = new MemoryStream(buffer, index, count))
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                string line = reader.ReadLine();
                
                if (line == null)
                {
                    throw new Exception("Malformed HTTP header");
                }

                if (ParseRequestLine(line) == false)
                {
                    throw new Exception($"Invalid HTTP method line: {line}");
                }

                HeaderLength += line.Length + 2;

                while (true)
                {
                    line = reader.ReadLine();

                    if (line == null)
                    {
                        throw new Exception("Malformed HTTP header");
                    }

                    HeaderLength += line.Length + 2;

                    if (line.Length == 0)
                    {
                        break;
                    }

                    if (ParseHeaderLine(line) == false)
                    {
                        throw new Exception($"Invalid HTTP header line. {line}");
                    }
                }
            }
        }

        public Header(HttpMethod method, string path, HttpStatusCode responseCode = 0, string command = null)
        {
            Method = method;
            Uri = new Uri(path, UriKind.Relative);
            Version = "HTTP/1.1";
            ResponseCode = responseCode;
            Message = command;
        }

        public Header(HttpStatusCode responseCode, string command = null)
        {
            Method = HttpMethod.StatusCode;
            Uri = new Uri(string.Empty, UriKind.Relative);
            Version = "HTTP/1.1";
            ResponseCode = responseCode;
            Message = command;
        }

        public Header Add(string name, string value)
        {
            header.Add(name.ToLowerInvariant(), value);

            return this;
        }

        public bool CheckValue(string name, string expectedValue, bool ignoreCase = false)
        {
            string value;

            if (header.TryGetValue(name.ToLowerInvariant(), out value) == false)
            {
                return expectedValue != null;
            }

            return value.Equals(expectedValue, ignoreCase ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture);
        }

        public bool Contains(string name)
        {
            return header.ContainsKey(name.ToLowerInvariant());
        }

        public void Remove(string name)
        {
            header.Remove(name.ToLowerInvariant());
        }

        public byte[] ToBytes()
        {
            return Encoding.UTF8.GetBytes(ToHeaderString());
        }

        public string ToHeaderString()
        {
            const string rn = "\r\n";
            StringBuilder sb = new StringBuilder();

            sb.Append(Method != HttpMethod.StatusCode ? $@"{Method} {Uri} {Version}" : $@"{Version}");

            if (ResponseCode != 0)
            {
                sb.Append($@" {(int)ResponseCode}");
            }

            if (Message != null)
            {
                sb.Append($@" {Message}");
            }

            sb.Append($@"{rn}");

            foreach (KeyValuePair<string, string> nameValue in header)
            {
                sb.Append($@"{nameValue.Key}: {nameValue.Value}{rn}");
            }

            // terminate the header
            sb.Append($@"{rn}");

            return sb.ToString();
        }

        public bool TryGetValue(string name, out string value)
        {
            return header.TryGetValue(name.ToLowerInvariant(), out value);
        }

        private static bool TryGetNextToken(string line, ref int index, out string token)
        {
            int end = line.IndexOf(' ', index);

            if (end > 0)
            {
                token = line.Substring(index, end - index);
                index = end + 1;

                return true;
            }

            if (end < 0 && line.Length - index > 0)
            {
                token = line.Substring(index);

                return true;
            }

            token = null;

            return false;
        }

        private bool ParseHeaderLine(string line)
        {
            string name, value;

            int index = 0;

            if (TryGetNextToken(line, ref index, out name) == false)
            {
                return false;
            }

            if (name[name.Length - 1] != ':')
            {
                return false;
            }

            if (TryGetNextToken(line, ref index, out value) == false)
            {
                value = "";
            }

            name = name.Substring(0, name.Length - 1);

            Add(name, value);

            return true;
        }

        private bool ParseRequestLine(string line)
        {
            string methodString, path = string.Empty, version, responseCodeString, message = null;
            HttpMethod method;
            int responseCode = -1;

            int index = 0;

            if (TryGetNextToken(line, ref index, out methodString) == false)
            {
                return false;
            }

            try
            {
                method = (HttpMethod)Enum.Parse(typeof(HttpMethod), methodString);

                if (method == HttpMethod.StatusCode)
                {
                    return false;
                }

                if (TryGetNextToken(line, ref index, out path) == false)
                {
                    return false;
                }

                if (TryGetNextToken(line, ref index, out version) == false)
                {
                    return false;
                }
            }
            catch
            {
                if (methodString.StartsWith("HTTP/") == false)
                {
                    return false;
                }

                method = HttpMethod.StatusCode;
                version = methodString;
            }

            if (TryGetNextToken(line, ref index, out responseCodeString) == true &&
                int.TryParse(responseCodeString, out responseCode) == true)
            {
            }
            else
            {
                if (method == HttpMethod.StatusCode)
                {
                    return false;
                }
            }

            if (line.Length > index)
            {
                message = line.Substring(index);
            }

            Method = method;
            Uri = new Uri(path, UriKind.Relative);
            Version = version;
            Message = message;
            ResponseCode = (HttpStatusCode)responseCode;

            return true;
        }

        public static byte[] GetResponse(HttpStatusCode statusCode, string message = null)
        {
            return new Header(statusCode, message).ToBytes(); 
        }

        //public static byte[] GetJsonResponse(Header header, string body)
        //{
        //    return GetMessageBytes(header, "application/json", body);
        //}

        //public static byte[] GetMessageBytes(Header header, string contentType, string body)
        //{
        //    return GetMessageBytes(header, contentType, Encoding.UTF8.GetBytes(body));
        //}

        //public static byte[] GetMessageBytes(Header header, string contentType, byte[] body)
        //{
        //    header.Add("Content-Type", contentType);
        //    header.Add("Content-Length", body.Length.ToString());

        //    using (MemoryStream buffer = new MemoryStream())
        //    {
        //        byte[] headerBytes = header.ToBytes();

        //        buffer.Write(headerBytes, 0, headerBytes.Length);
        //        buffer.Write(body, 0, body.Length);

        //        return buffer.ToArray();
        //    }
        //}
    }
}