﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Runtime.InteropServices;

namespace Nano.Transport.Streams
{
    public static class MemoryHelper
    {
        #region General Purpose 

        [DllImport("msvcrt.dll", SetLastError = false)]
        private static extern IntPtr memcpy(IntPtr dest, IntPtr src, int count);

        public static void CopyStructArray<I, J>(I[] source, J[] destination, int index, int count)
            where I : struct
            where J : struct
        {
            int sizeI = Marshal.SizeOf(typeof(I));
            int sizeJ = Marshal.SizeOf(typeof(J));

            if (sizeI != sizeJ)
            {
                throw new Exception($"Incompatible types {typeof(I)} and {typeof(J)}.");
            }

            if (index < 0)
            {
                throw new IndexOutOfRangeException();
            }

            if (source.Length < index + count)
            {
                throw new IndexOutOfRangeException("Source array is too small");
            }

            if (destination.Length < index + count)
            {
                throw new IndexOutOfRangeException("Destination array is too small");
            }

            GCHandle sourceHandle = GCHandle.Alloc(source, GCHandleType.Pinned);
            GCHandle destinationHandle = GCHandle.Alloc(destination, GCHandleType.Pinned);

            int offset = index * sizeI;

            try
            {
                memcpy(new IntPtr(destinationHandle.AddrOfPinnedObject().ToInt64() + offset), new IntPtr(sourceHandle.AddrOfPinnedObject().ToInt64() + offset), count * sizeI);
            }
            finally
            {
                sourceHandle.Free();
                destinationHandle.Free();
            }
        }

        #endregion

        #region Write

        public static byte[] GetBytes<T>(T value, ref int index) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            byte[] bytes = new byte[size];

            GCHandle handle = GCHandle.Alloc(value, GCHandleType.Pinned);

            try
            {
                Marshal.Copy(handle.AddrOfPinnedObject(), bytes, index, size);
            }
            finally
            {
                handle.Free();
            }

            index += size;

            return bytes;
        }

        public static byte[] GetBytes<T>(ref T value, ref int index) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            byte[] bytes = new byte[size];

            GCHandle handle = GCHandle.Alloc(value, GCHandleType.Pinned);

            try
            {
                Marshal.Copy(handle.AddrOfPinnedObject(), bytes, index, size);
            }
            finally
            {
                handle.Free();
            }

            index += size;

            return bytes;
        }

        public static void GetBytes<T>(T value, byte[] bytes, ref int index) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));

            GCHandle handle = GCHandle.Alloc(value, GCHandleType.Pinned);

            try
            {
                Marshal.Copy(handle.AddrOfPinnedObject(), bytes, index, size);
            }
            finally
            {
                handle.Free();
            }

            index += size;
        }

        public static void GetBytes<T>(ref T value, byte[] bytes, ref int index) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));

            GCHandle handle = GCHandle.Alloc(value, GCHandleType.Pinned);

            try
            {
                Marshal.Copy(handle.AddrOfPinnedObject(), bytes, index, size);
            }
            finally
            {
                handle.Free();
            }            

            index += size;
        }

        public static byte[] GetArrayBytes<T>(T[] array, ref int index) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size * array.Length;
            byte[] bytes = new byte[totalSize];

            GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);

            try
            {
                Marshal.Copy(handle.AddrOfPinnedObject(), bytes, index, totalSize);
            }
            finally
            {
                handle.Free();
            }

            index += totalSize;

            return bytes;
        }

        public static void GetArrayBytes<T>(T[] array, int count, byte[] bytes, ref int index)
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size * count;

            GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);

            try
            {
                Marshal.Copy(handle.AddrOfPinnedObject(), bytes, index, totalSize);
            }
            finally
            {
                handle.Free();
            }

            index += totalSize;
        }

        #endregion

        #region Read

        public static T FromBytes<T>(byte[] bytes, ref int index) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size;

            T result = default(T);

            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);

            try
            {
                result = (T)Marshal.PtrToStructure((IntPtr)(handle.AddrOfPinnedObject().ToInt64() + index), typeof(T));
            }
            finally
            {
                handle.Free();
            }

            index += size;

            return result;
        }

        public static void FromBytes<T>(byte[] bytes, ref int index, ref T result) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size;

            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);

            try
            {
                result = (T)Marshal.PtrToStructure((IntPtr)(handle.AddrOfPinnedObject().ToInt64() + index), typeof(T));
            }
            finally
            {
                handle.Free();
            }

            index += size;
        }

        public static T[] ArrayFromBytes<T>(byte[] bytes, int count) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size * count;

            T[] result = new T[count];

            GCHandle handle = GCHandle.Alloc(result, GCHandleType.Pinned);

            try
            {
                Marshal.Copy(bytes, 0, handle.AddrOfPinnedObject(), totalSize);
            }
            finally
            {
                handle.Free();
            }

            return result;
        }

        public static T[] ArrayFromBytes<T>(byte[] bytes, ref int index, int count) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size * count;

            T[] result = new T[count];

            GCHandle handle = GCHandle.Alloc(result, GCHandleType.Pinned);

            try
            {
                Marshal.Copy(bytes, index, handle.AddrOfPinnedObject(), totalSize);
            }
            finally
            {
                handle.Free();
            }

            index += size * count;

            return result;
        }

        public static void ArrayFromBytes<T>(byte[] bytes, ref int index, T[] data, int count) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);

            try
            {
                int size = Marshal.SizeOf(typeof(T));
                int totalSize = size * count;

                Marshal.Copy(bytes, index, handle.AddrOfPinnedObject(), totalSize);

                index += size * count;
            }
            finally
            {
                handle.Free();
            }
        }

        public static void ArrayFromBytes<T>(byte[] bytes, ref int index, T[] data, int count, IntPtr temp)
        {
            GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);

            try 
            {
                int size = Marshal.SizeOf(typeof(T));
                int totalSize = size * count;

                Marshal.Copy(bytes, index, handle.AddrOfPinnedObject(), totalSize);                

                index += size * count;
            }
            finally
            {
                handle.Free();
            }
        }

        #endregion

        #region Write (Instance Wise)

        public static byte[] GetBytes_InstanceWise<T>(T value, ref int index) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            byte[] bytes = new byte[size];

            IntPtr pointer = Marshal.AllocHGlobal(size);

            Marshal.StructureToPtr(value, pointer, true);

            Marshal.Copy(pointer, bytes, index, size);

            Marshal.FreeHGlobal(pointer);

            index += size;

            return bytes;
        }

        public static void GetBytes_InstanceWise<T>(T value, byte[] bytes, ref int index) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));

            IntPtr pointer = Marshal.AllocHGlobal(size);

            Marshal.StructureToPtr(value, pointer, true);

            Marshal.Copy(pointer, bytes, index, size);

            Marshal.FreeHGlobal(pointer);

            index += size;
        }

        public static byte[] GetArrayBytes_InstanceWise<T>(T[] array, ref int index) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size * array.Length;
            byte[] bytes = new byte[totalSize];

            IntPtr pointer = Marshal.AllocHGlobal(totalSize);
            long longPtr = pointer.ToInt64();

            IntPtr ptr = (IntPtr)(longPtr);

            foreach (T item in array)
            {
                Marshal.StructureToPtr(item, ptr, true);

                longPtr += size;

                ptr = (IntPtr)(longPtr);
            }

            Marshal.Copy(pointer, bytes, index, totalSize);

            Marshal.FreeHGlobal(pointer);

            index += totalSize;

            return bytes;
        }

        public static void GetArrayBytes_InstanceWise<T>(T[] array, int count, byte[] bytes, ref int index)
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size * count;

            IntPtr pointer = Marshal.AllocHGlobal(totalSize);
            long longPtr = pointer.ToInt64();

            IntPtr ptr = (IntPtr)(longPtr);

            for (int i = 0; i < count; i++)
            {
                Marshal.StructureToPtr(array[i], ptr, true);

                longPtr += size;

                ptr = (IntPtr)(longPtr);
            }

            Marshal.Copy(pointer, bytes, index, totalSize);

            Marshal.FreeHGlobal(pointer);

            index += totalSize;
        }

        #endregion 

        #region Read (Instance Wise)

        public static T FromBytes_InstanceWise<T>(byte[] array, ref int index) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));

            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(array, index, ptr, size);

            T str = (T)Marshal.PtrToStructure(ptr, typeof(T));

            Marshal.FreeHGlobal(ptr);

            index += size;

            return str;
        }

        public static T[] ArrayFromBytes_InstanceWise<T>(byte[] bytes, int count) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size * count;

            IntPtr pointer = Marshal.AllocHGlobal(totalSize);
            long longPtr = pointer.ToInt64();

            IntPtr ptr = (IntPtr)(longPtr);

            Marshal.Copy(bytes, 0, pointer, totalSize);

            T[] result = new T[count];

            for (int i = 0; i < count; i++)
            {
                result[i] = (T)Marshal.PtrToStructure(ptr, typeof(T));

                longPtr += size;

                ptr = (IntPtr)(longPtr);
            }

            Marshal.FreeHGlobal(pointer);

            return result;
        }

        public static T[] ArrayFromBytes_InstanceWise<T>(byte[] bytes, ref int index, int count) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size * count;

            IntPtr pointer = Marshal.AllocHGlobal(totalSize);
            long longPtr = pointer.ToInt64();

            IntPtr ptr = (IntPtr)(longPtr);

            Marshal.Copy(bytes, index, pointer, totalSize);

            T[] result = new T[count];

            for (int i = 0; i < count; i++)
            {
                result[i] = (T)Marshal.PtrToStructure(ptr, typeof(T));

                longPtr += size;

                ptr = (IntPtr)(longPtr);
            }

            Marshal.FreeHGlobal(pointer);

            index += size * count;

            return result;
        }

        public static void ArrayFromBytes_InstanceWise<T>(byte[] bytes, ref int index, T[] data, int count) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size * count;

            IntPtr pointer = Marshal.AllocHGlobal(totalSize);
            long longPtr = pointer.ToInt64();

            IntPtr ptr = (IntPtr)(longPtr);

            Marshal.Copy(bytes, index, pointer, totalSize);

            for (int i = 0; i < count; i++)
            {
                data[i] = (T)Marshal.PtrToStructure(ptr, typeof(T));

                longPtr += size;

                ptr = (IntPtr)(longPtr);
            }

            Marshal.FreeHGlobal(pointer);

            index += size * count;
        }

        public static void ArrayFromBytes_InstanceWise<T>(byte[] bytes, ref int index, T[] data, int count, IntPtr temp)
        {
            int size = Marshal.SizeOf(typeof(T));
            int totalSize = size * count;

            long longPtr = temp.ToInt64();

            IntPtr ptr = (IntPtr)(longPtr);

            Marshal.Copy(bytes, index, temp, totalSize);

            for (int i = 0; i < count; i++)
            {
                data[i] = (T)Marshal.PtrToStructure(ptr, typeof(T));

                longPtr += size;

                ptr = (IntPtr)(longPtr);
            }

            index += size * count;
        }

        #endregion
    }
}
