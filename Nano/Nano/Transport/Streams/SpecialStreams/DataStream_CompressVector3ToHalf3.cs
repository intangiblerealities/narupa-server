﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using SlimMath;

namespace Nano.Transport.Streams.SpecialStreams
{
    public class DataStream_CompressVector3ToHalf3 : IDataStream<Vector3>
    {
        IDataStream<Half3> destinationStream;

        public int Count
        {
            get => destinationStream.Count;
            set => destinationStream.Count = value;
        }

        public Vector3[] Data { get; }

        public TransportStreamFormatDescriptor Format => destinationStream.Format;

        public ushort ID => destinationStream.ID;

        public int MaxCount => destinationStream.MaxCount;

        public int MaxSizeInBytes => destinationStream.MaxSizeInBytes;

        public string Name => destinationStream.Name;

        public DataStream_CompressVector3ToHalf3(IDataStream<Half3> destinationStream)
        {
            this.destinationStream = destinationStream;

            Data = new Vector3[destinationStream.MaxCount];
        }

        public void FlushToDestinationStream()
        {
            Vector3[] source = Data;
            Half3[] destination = destinationStream.Data;

            for (int i = 0; i < source.Length; i++)
            {
                destination[i] = (Half3)source[i];
            }
        }

        public void CopyFrom(byte[] bytes, ref int index)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(byte[] bytes, ref int index)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            destinationStream.Dispose();
        }
    }

}
