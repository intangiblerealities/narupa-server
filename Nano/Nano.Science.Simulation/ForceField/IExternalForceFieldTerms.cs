﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;

namespace Nano.Science.Simulation.ForceField
{
    /// <summary>
    /// Interface <see cref="IExternalForceFieldTerms"/> representing the data required to create the corresponding <see cref="IForceField"/>.
    /// </summary>
    /// <seealso cref="Nano.Loading.ILoadable" />
    /// <remarks>
    /// This interface is used to load external forcefields that are created after templates and residues have been spawned.
    /// </remarks>
    public interface IExternalForceFieldTerms : ILoadable
    {
        /// <summary>
        /// Instantiates the force field terms into the instantiateed topology.
        /// </summary>
        /// <param name="parentTopology">The parent topology.</param>
        void AddToTopology(InstantiatedTopology parentTopology);
    }
}