﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Loading;
using System.Xml;

namespace Nano.Science.Simulation.Integrator
{
    /// <summary>
    /// The base integrator properties class, from which other integrators derive.
    /// </summary>
    /// <remarks>
    /// The base integrator is used by the builder to automate the selection of an appropriate integrator.
    /// </remarks>
    /// <seealso cref="IIntegratorProperties" />
    [XmlName("IntegratorBase")]
    public class IntegratorPropertiesBase : IIntegratorProperties
    {
        /// <summary>
        /// Gets or sets the time step.
        /// </summary>
        /// <value>The time step.</value>
        public float TimeStep { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public virtual string Name => "IntegratorBase";

        /// <summary>
        /// Gets or sets the time step.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="reporter">The reporter.</param>
        /// <returns>IIntegrator.</returns>
        /// <exception cref="Exception">The integrator base cannot be initialised as an full integrator.</exception>
        public virtual IIntegrator InitialiseIntegrator(IAtomicSystem system, IReporter reporter)
        {
            throw new Exception("The integrator base cannot be initialised as an full integrator.");
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>IIntegratorProperties.</returns>
        public virtual IIntegratorProperties Clone()
        {
            IntegratorPropertiesBase b = new IntegratorPropertiesBase
            {
                TimeStep = TimeStep
            };

            return b;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        public virtual bool Equals(IIntegratorProperties other)
        {
            if (other is IntegratorPropertiesBase == false) return false;
            IntegratorPropertiesBase b = other as IntegratorPropertiesBase;
            if (b.Name != Name) return false;
            if (Math.Abs(b.TimeStep - TimeStep) > 0.00001f) return false;
            return true;
        }

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public virtual void Load(LoadContext context, XmlNode node)
        {
            TimeStep = Helper.GetAttributeValue(node, "TimeStep", 0.5f);
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public virtual XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "TimeStep", TimeStep);
            return element;
        }
    }
}