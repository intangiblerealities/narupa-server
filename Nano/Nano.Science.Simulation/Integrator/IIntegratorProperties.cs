﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using System;

namespace Nano.Science.Simulation.Integrator
{
    /// <summary>
    /// Interface IIntegratorProperties representing the properties of the corresponding <see cref="IIntegrator"/>.
    /// </summary>
    /// <seealso cref="Nano.Loading.ILoadable" />
    public interface IIntegratorProperties : ILoadable, IEquatable<IIntegratorProperties>
    {
        /// <summary>
        /// Name of this integrator.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>IIntegratorProperties.</returns>
        IIntegratorProperties Clone();

        /// <summary>
        /// Initialises the integrator from the integrator properties.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="reporter">The reporter.</param>
        /// <returns>IIntegrator.</returns>
        IIntegrator InitialiseIntegrator(IAtomicSystem system, IReporter reporter);
    }
}