﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using Nano.Transport.OSCCommands;
using Nano.Transport.Variables;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Interface representing a molecular dynamics simulation.
    /// </summary>
    public interface ISimulation : ILoadable, IVariableProvider, ICommandProvider
    {
        
        /// <summary>
        /// Name of the simulation.
        /// </summary>
        string Name { get; }
        /// <summary>
        /// The atomic system.
        /// </summary>
        IAtomicSystem ActiveSystem { get; }

        /// <summary>
        /// The active topology.
        /// </summary>
        InstantiatedTopology ActiveTopology { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has reset.
        /// </summary>
        /// <value><c>true</c> if this instance has reset; otherwise, <c>false</c>.</value>
        bool HasReset { get; }

        /// <summary>
        /// Gets the simulation time.
        /// </summary>
        /// <value>The simulation time.</value>
        float SimulationTime { get; }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        void Reset();

        /// <summary>
        /// Runs the specified steps.
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <param name="interactionPoints">The interaction points.</param>
        void Run(int steps, List<Interaction> interactionPoints = null);

        /// <summary>
        /// Pause the simulation.
        /// </summary>
        /// <remarks>
        /// If the simulation is not something controlled by the Simbox Server directly, this is used to communicate
        /// a pause signal.
        /// </remarks>
        void Pause();

        /// <summary>
        /// Has the end of the simulation been reached.
        /// </summary>
        bool EndOfSimulation { get; } 
    }
}