﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Agents;
using Nano.Transport.Variables;
using System;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Abstract class representing basic properties of a barostat.
    /// </summary>
    /// <seealso cref="Nano.Science.Simulation.IThermostat" />
    /// <seealso cref="Nano.Transport.Variables.IVariableProvider" />
    public abstract class Barostat : IThermostat, IVariableProvider
    {
        #region Public Properties

        /// <summary>
        /// The equilibrium pressure in bars.
        /// </summary>
        /// <value>The equilibrium pressure.</value>
        public float EquilibriumPressure { get; protected set; }

        /// <summary>
        /// The maximum pressure.
        /// </summary>
        /// <value>The maximum pressure.</value>
        public float MaximumPressure { get; protected set; }

        /// <summary>
        /// The minimum pressure.
        /// </summary>
        /// <value>The minimum pressure.</value>
        public float MinimumPressure { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="IVariableProvider" />  is initialised
        /// </summary>
        /// <value><c>true</c> if the IVariableProvider; otherwise, <c>false</c>.</value>
        public bool VariablesInitialised { get; protected set; }

        /// <inheritdoc />
        public abstract string Name { get; }

        #endregion Public Properties

        /// <summary>
        /// The equilibrium pressure IVariable
        /// </summary>
        public IVariable<float> EquilibriumPressureIVar;

        /// <summary>
        /// The minimum pressure ivariable
        /// </summary>
        protected IVariable<float> MinimumPressureIVar;

        /// <summary>
        /// The maximum pressure ivariable
        /// </summary>
        protected IVariable<float> MaximumPressureIVar;

        /// <summary>
        /// Occurs when [temperature changed].
        /// </summary>
        public event EventHandler TemperatureChanged;

        #region Public Methods

        /// <summary>
        /// Sets the equilibrium pressure.
        /// </summary>
        /// <param name="v">The new desired pressure.</param>
        public void SetEquilibriumPressure(float v)
        {
            EquilibriumPressure = v;
            if (EquilibriumPressureIVar != null)
            {
                EquilibriumPressureIVar.DesiredValue = v;
            }
        }

        /// <summary>
        /// Broadcasts this instance's IVariables.
        /// </summary>
        public virtual void ResetVariableDesiredValues()
        {
            if (!VariablesInitialised) return;
            EquilibriumPressureIVar.DesiredValue = EquilibriumPressure;
        }

        /// <summary>
        /// Disposes this instance's IVariables.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialises this instance's IVariables.
        /// </summary>
        /// <param name="variableManager">The variable manager.</param>
        public virtual void InitialiseVariables(VariableManager variableManager)
        {
            EquilibriumPressureIVar = variableManager.Variables.Add(VariableName.Pressure, VariableType.Float) as IVariable<float>;

            VariablesInitialised = true;

            ResetVariableDesiredValues();
        }

        /// <summary>
        /// Updates this instance's IVariables.
        /// </summary>
        public virtual void UpdateVariableValues()
        {
            if (!VariablesInitialised) return;
            if (EquilibriumPressureIVar.DesiredValue < MinimumPressure)
            {
                EquilibriumPressureIVar.DesiredValue = MinimumPressure;
            }
            else if (EquilibriumPressureIVar.DesiredValue >= MaximumPressure)
            {
                EquilibriumPressureIVar.DesiredValue = MaximumPressure;
            }
            if (EquilibriumPressureIVar.DesiredValue != EquilibriumPressure)
            {
                EquilibriumPressure = EquilibriumPressureIVar.DesiredValue;
            }
        }

        /// <summary>
        /// Applies to the specified system.
        /// </summary>
        /// <param name="system">The system.</param>
        public abstract void ApplyStat(IAtomicSystem system);

        /// <summary>
        /// Gets the properties of this thermostat.
        /// </summary>
        /// <returns>IThermostatProperties.</returns>
        public abstract IThermostatProperties GetProperties();

        #endregion Public Methods
    }
}