﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Agents;
using Nano.Transport.Variables;
using System;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Abstract class representing basic properties of a thermostat.
    /// </summary>
    public abstract class Thermostat : IThermostat, IVariableProvider
    {
        #region Public Properties

        /// <summary>
        /// The equilibrium temperature.
        /// </summary>
        /// <value>The equilibrium temperature.</value>
        public float EquilibriumTemperature;

        /// <summary>
        /// The maximum temperature.
        /// </summary>
        /// <value>The maximum temperature.</value>
        public float MaximumTemperature;

        /// <summary>
        /// The minimum temperature.
        /// </summary>
        /// <value>The minimum temperature.</value>
        public float MinimumTemperature;

        /// <value><c>true</c> if the IVariableProvider; otherwise, <c>false</c>.</value>
        public bool VariablesInitialised { get; protected set; }

        /// <inheritdoc />
        public abstract string Name { get; }

        /// <summary>
        /// The instantaneous temperature of the system.
        /// </summary>
        public float InstantaneousTemperature;

        #endregion Public Properties

        /// <summary>
        /// The equilibrium temperature IVariable
        /// </summary>
        public IVariable<float> EquilibriumTemperatureIVar;

        /// <summary>
        /// The minimum temperature <see cref="IVariable"/>.
        /// </summary>
        protected IVariable<float> MinimumTemperatureIVar;

        /// <summary>
        /// The maximum temperature <see cref="IVariable"/>.
        /// </summary>
        protected IVariable<float> MaximumTemperatureIVar;

        /// <inheritdoc />
        public event EventHandler TemperatureChanged;

        #region Public Methods

        /// <summary>
        /// Sets the equilibrium temperature.
        /// </summary>
        /// <param name="v">The new desired temperature.</param>
        public void SetEquilibriumTemperature(float v)
        {
            EquilibriumTemperature = v;
            if (EquilibriumTemperatureIVar != null)
            {
                EquilibriumTemperatureIVar.DesiredValue = v;
            }
            TemperatureChanged?.Invoke(this, null);
        }

        /// <summary>
        /// Resets this instance's IVariables' desired values.
        /// </summary>
        public virtual void ResetVariableDesiredValues()
        {
            EquilibriumTemperatureIVar.DesiredValue = EquilibriumTemperature;
        }

        /// <summary>
        /// Disposes this instance's IVariables.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual void Dispose()
        {
        }

        /// <summary>
        /// Initialises this instance's IVariables.
        /// </summary>
        /// <param name="variableManager">The variable manager.</param>
        public virtual void InitialiseVariables(VariableManager variableManager)
        {
            EquilibriumTemperatureIVar = variableManager.Variables.Add(VariableName.Temperature, VariableType.Float) as IVariable<float>;
            VariablesInitialised = true;
        }

        /// <summary>
        /// Updates this instance's IVariables.
        /// </summary>
        public virtual void UpdateVariableValues()
        {
            if (EquilibriumTemperatureIVar.DesiredValue < MinimumTemperature)
            {
                EquilibriumTemperatureIVar.DesiredValue = MinimumTemperature;
            }
            else if (EquilibriumTemperatureIVar.DesiredValue >= MaximumTemperature)
            {
                EquilibriumTemperatureIVar.DesiredValue = MaximumTemperature;
            }

            if (EquilibriumTemperatureIVar.DesiredValue != EquilibriumTemperature)
            {
                EquilibriumTemperature = EquilibriumTemperatureIVar.DesiredValue;
            }
        }

        /// <summary>
        /// Applies the thermostat to the system.
        /// </summary>
        public abstract void ApplyStat(IAtomicSystem system);

        /// <inheritdoc />
        public abstract IThermostatProperties GetProperties();

        #endregion Public Methods
    }
}