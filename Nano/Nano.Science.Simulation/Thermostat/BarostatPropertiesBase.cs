﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using System.Xml;
using Nano.Science.Simulation.Integrator;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Base representation of thermostat properties.
    /// </summary>
    /// <seealso cref="Nano.Science.Simulation.IThermostatProperties" />
    public abstract class BarostatPropertiesBase : IThermostatProperties
    {
        /// <summary>
        /// The equilibrium pressure in bars.
        /// </summary>
        public float EquilibriumPressure;

        /// <summary>
        /// The maximum pressure in bars.
        /// </summary>
        public float MaximumPressure;

        /// <summary>
        /// The minimum pressure in bars.
        /// </summary>
        public float MinimumPressure;

        /// <inheritdoc />
        public abstract string Name { get; }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>IThermostatProperties.</returns>
        public abstract IThermostatProperties Clone();

        /// <summary>
        /// Determines whether this barostat is equivalent to the other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public abstract bool Equals(IThermostatProperties other);

        /// <inheritdoc />
        public abstract IThermostat InitialiseStat(IAtomicSystem system, IIntegrator integrator);

        /// <inheritdoc />
        public virtual void Load(LoadContext context, XmlNode node)
        {
            EquilibriumPressure = Helper.GetAttributeValue(node, "EquilibriumPressure", 1.0f);
            MaximumPressure = Helper.GetAttributeValue(node, "MaximumPressure", 1000000f);
            MinimumPressure = Helper.GetAttributeValue(node, "MinimumPressure", 0.01f);
        }

        /// <inheritdoc />
        public virtual XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "EquilibriumPressure", EquilibriumPressure);
            Helper.AppendAttributeAndValue(element, "MaximumPressure", MaximumPressure);
            Helper.AppendAttributeAndValue(element, "EquilibriumPressure", EquilibriumPressure);

            return element;
        }
    }
}