﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Interface for the implementation of x-stats, e.g. Thermostat and Barostats.
    /// </summary>
    public interface IThermostat

    {
        /// <summary>
        /// Gets the name of the thermostat.
        /// </summary>
        /// <value>The name.</value>
        string Name { get; }

        /// <summary>
        /// Occurs when [temperature changed].
        /// </summary>
        event EventHandler TemperatureChanged;

        /// <summary>
        /// Applies to the specified system.
        /// </summary>
        /// <param name="system">The system.</param>
        void ApplyStat(IAtomicSystem system);

        /// <summary>
        /// Gets the properties of this thermostat.
        /// </summary>
        /// <returns>IThermostatProperties.</returns>
        IThermostatProperties GetProperties();
    }
}