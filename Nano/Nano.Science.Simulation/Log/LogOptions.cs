﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;

namespace Nano.Science.Simulation.Log
{
    /// <summary>
    /// Log Options for a molecular dynamics trajectory logger.
    /// </summary>
    /// <remarks>
    /// When loaded in an <see cref="ISimulationLogger"/>, each option is loaded as an individual boolean flag.
    /// </remarks>
    /// <example>
    /// An example usage with the <see cref="XYZLogger"/>:
    /// <code>
    /// XYZLogger Positions=True Velocities=False
    /// </code>
    /// 
    /// </example>
    [Flags]
    public enum LogOptions
    {
        /// <summary>
        /// No logging of state.
        /// </summary>
        None = 0,
        /// <summary>
        /// Log positions.
        /// </summary>
        Positions = 1,
        /// <summary>
        /// Log velocities.
        /// </summary>
        Velocities = Positions << 1,
        /// <summary>
        /// Log forces.
        /// </summary>
        Forces = Velocities << 1,
        /// <summary>
        /// Log interactions.
        /// </summary>
        Interactions = Forces << 1
    }
}