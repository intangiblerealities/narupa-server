﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Spawning;
using SlimMath;

namespace Nano.Science.Simulation.Residues
{
    /// <summary>
    /// Represents an atom in an unspawned topology.
    /// </summary>
    public interface IAtom : ILoadable
    {
        /// <summary>
        /// Element of atom.
        /// </summary>
        Element Element { get; set; }

        /// <summary>
        /// Name of atom.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Position of atom.
        /// </summary>
        Vector3 Position { get; set; }

        /// <summary>
        /// Add the atom to the given topology.
        /// </summary>
        /// <param name="topology">Topology to add atom to.</param>
        /// <param name="context">Context in which to spawn atom.</param>
        /// <returns></returns>
        int AddToTopology(InstantiatedTopology topology, ref SpawnContext context);
    }
}