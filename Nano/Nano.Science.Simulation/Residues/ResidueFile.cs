﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Spawning;
using SlimMath;

namespace Nano.Science.Simulation.Residues
{
    /// <summary>
    /// Represents an atom group to be loaded from a template file.
    /// </summary>
    [XmlName("ResidueFile")]
    public class ResidueFile : IResidue
    {
        private IResidue residueFromFile;

        /// <inheritdoc />
        public List<IAtom> Atoms { get { return residueFromFile.Atoms; } }

        /// <inheritdoc />
        public List<IBond> Bonds { get { return residueFromFile.Bonds; } }

        /// <inheritdoc />
        public ResidueCollection Children { get { return residueFromFile.Children; } }

        /// <summary>
        /// Path to template.
        /// </summary>
        public string FilePath { get; set; }

        /// <inheritdoc />
        public int FirstAtomIndexWithParent { get; set; }

        /// <inheritdoc />
        public int Id { get; set; }

        
        /// <summary>
        /// Name of this atom group.
        /// </summary>
        public string Name { get; set; }

        /// <inheritdoc />
        public IResidue Parent { get { return residueFromFile.Parent; } set { residueFromFile.Parent = value; } }

        /// <inheritdoc />
        public int TotalAtomCount { get { return residueFromFile.TotalAtomCount; } }

        /// <inheritdoc />
        public void CheckLinkage()
        {
            residueFromFile.CheckLinkage();
        }

        /// <inheritdoc />
        public BoundingBox GetBoundingBox(ref SpawnContext context)
        {
            return residueFromFile.GetBoundingBox(ref context);
        }

        /// <inheritdoc />
        public bool Intersects(List<BoundingSphere> boundingSpheres, ref SpawnContext context)
        {
            return residueFromFile.Intersects(boundingSpheres, ref context);
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, "Name", string.Empty);
            Id = Helper.GetAttributeValue(node, "ID", 0);

            FilePath = Helper.GetAttributeValue(node, "FilePath", null);

            if (string.IsNullOrEmpty(FilePath) == true)
            {
                throw new FlexibleFileFormatException($@"Missing FilePath attribute on ResidueFile node ""{Name}"".", node.OuterXml);
            }

            if (File.Exists(Helper.ResolvePath(FilePath)) == false)
            {
                throw new FlexibleFileFormatException($@"The file path ""{FilePath}"" does not exist or is an invalid path.", node.OuterXml);
            }

            residueFromFile = ResidueUtils.LoadFromFile(context, FilePath);

            if (residueFromFile == null)
            {
                throw new FlexibleFileFormatException($@"The file path ""{FilePath}"" does not contain a valid residue.", node.OuterXml);
            }
        }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", Name);
            Helper.AppendAttributeAndValue(element, "FilePath", FilePath);

            return element;
        }

        /// <inheritdoc />
        public void Spawn(ISimulationBoundary box, List<BoundingSphere> boundingSpheres, InstantiatedTopology topology, ref SpawnContext context)
        {
            residueFromFile.Spawn(box, boundingSpheres, topology, ref context);
        }
    }
}