﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Loading;

namespace Nano.Server
{
    public interface IServiceEndpoint : ILoadable
    {
        Uri EndpointUri { get; }   
    }
}