﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Text;

namespace Nano.Server.Plugins
{
	public static class LibraryPathManager
	{
		static readonly List<string> paths = new List<string>();
		static readonly string originalPath;

		static LibraryPathManager()
		{
			originalPath = GetPath ();
		}

		/// <summary>
		/// Add a path to the PATH environment variable.
		/// </summary>
		/// <param name="path">An absolute or '^/' style relative path.</param>
		public static void Add(string path)
		{
			if (paths.Contains(path) == true)
			{
				return; 
			}

			paths.Add(path);

			Update(); 
		}



		/// <summary>
		/// Remove a path from the PATH environment variable.
		/// </summary>
		/// <param name="path">An absolute or '^/' style relative path.</param>
		public static void Remove(string path)
		{
			if (paths.Contains(path) == false)
			{
				return;
			}

			paths.Remove(path);

			Update();
		}

		static void Update()
		{
			StringBuilder sb = new StringBuilder();


			foreach (string path in paths)
			{
				sb.AppendFormat("{0};", Helper.ResolvePath(path)); 
			}

            sb.Append(originalPath);

            SetPath(sb.ToString ());
		}

		public static string GetPath()
		{
			string path;
			switch (PlatformHelper.RunningPlatform ()) 
			{
			case PlatformHelper.Platform.Linux:
				path = Environment.GetEnvironmentVariable ("LD_LIBRARY_PATH");
				break;
			case PlatformHelper.Platform.OSX:
				path = Environment.GetEnvironmentVariable ("LD_LIBRARY_PATH");
				break;
			default:
				path = Environment.GetEnvironmentVariable ("PATH");
				break;
			}
			return path;
		}

		private static void SetPath(string value)
		{
			switch (PlatformHelper.RunningPlatform ()) 
			{
			case PlatformHelper.Platform.Linux:
				Environment.SetEnvironmentVariable ("LD_LIBRARY_PATH", value);
				break;
			case PlatformHelper.Platform.OSX:
				Environment.SetEnvironmentVariable ("DYLD_LIBRARY_PATH", value);
				break;
			default:
				Environment.SetEnvironmentVariable ("PATH", value);
				break;
			}

		}
	}
}

