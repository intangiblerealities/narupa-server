﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Server
{
    public enum ServerOnConnectionState
    {
        Play,
        Pause,
        RestartThenPlay,
        RestartThenPause
    }
}