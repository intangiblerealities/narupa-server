﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Nano.Loading;
using Rug.Cmd;

namespace Nano.Server.Basic
{
    [XmlName("DumyMonitor")]
    public class DumyMonitor : IMonitor
    {
        // sudo docker stats --all --no-stream --format "table {{.Container}}\t{{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}\t{{.NetIO}}\t{{.BlockIO}}"
        
        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            return element;
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
        }

        /// <inheritdoc />
        public void RegisterArguments(ArgumentParser parser)
        {
        }

        /// <inheritdoc />
        public void ValidateArguments(IReporter reporter)
        {
        }

        /// <inheritdoc />
        public async Task Start(IServer serverContext, CancellationToken cancel)
        {
            IReporter reporter = serverContext.ReportManager.BeginReport(ReportContext.Monitor, "dumy-monitor", out Uri reportIdentifier);

            try
            {
                reporter.PrintNormal(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "dumy-monitor started");
                
                do
                {
                    await Task.Delay(60000, cancel); 
                }
                while (cancel.IsCancellationRequested == false); 
            }
            catch (TaskCanceledException _)
            {
                reporter.PrintDebug(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "dumy-monitor task canceled"); 
            }
            finally
            {
                reporter.PrintNormal(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "dumy-monitor ended");
                
                serverContext.ReportManager.EndReport(reportIdentifier); 
            }
        }
    }
}