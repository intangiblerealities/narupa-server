﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Xml;
using Nano.Loading;
using Rug.Cmd;

namespace Nano.Server.Basic
{
    [XmlName("ServiceEndpoint")] 
    public class ServiceEndpoint : IServiceEndpoint, IServerComponent
    {
        private StringArgument endpointArgument = new StringArgument("service-endpoint", "The end-point uri that the service exists at", "The end-point uri that the service exists at");  
        
        /// <inheritdoc />
        public Uri EndpointUri { get; set; }
        
        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(EndpointUri), EndpointUri.ToString());             

            return element; 
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            EndpointUri = new Uri(Helper.GetAttributeValue(node, nameof(EndpointUri), "tcp://0.0.0.0:8000"), UriKind.Absolute); 
        }

        /// <inheritdoc />
        public void RegisterArguments(ArgumentParser parser)
        {
            endpointArgument.Reset(); 
            
            parser.Add("-", "Endpoint", endpointArgument); 
        }

        /// <inheritdoc />
        public void ValidateArguments(IReporter reporter)
        {
            if (endpointArgument.Defined == false && EndpointUri == null)
            {
                throw new ArgumentException("No service endpoint defined"); 
            }
            
            if (endpointArgument.Defined == false)
            {
                return; 
            }

            EndpointUri = new Uri(endpointArgument.Value); 
        }
    }
}