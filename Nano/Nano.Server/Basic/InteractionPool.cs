﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano.Transport.Variables.Interaction;
using SlimMath;
using Interaction = Nano.Science.Simulation.Interaction;

namespace Nano.Server.Basic
{
    /// <summary>
    /// Represents a pool of possible interactions.
    /// </summary>
    public class InteractionPool
    {
        /// <summary>
        /// The maximum number of interactions.
        /// </summary>
        public const int MAX_INTERACTIONS = 256;

        /// <summary>
        /// The interaction points.
        /// </summary>
        public readonly List<Interaction> Interactions;

        /// <summary>
        /// Gets the current count of interaction points.
        /// </summary>
        /// <value>The current count.</value>
        public int CurrentCount => Interactions.Count;

        /// <summary>
        /// Initializes a new instance of the <see cref="InteractionPool"/> class.
        /// </summary>
        public InteractionPool()
        {
            Interactions = new List<Interaction>(MAX_INTERACTIONS);
        }

        /// <summary>
        /// Clears this instance of interaction points.
        /// </summary>
        public void Clear()
        {
            Interactions.Clear();
        }

        public void CopyInteractionsFrom(Nano.Transport.Variables.Interaction.Interaction[] data, int count, int[] atomSelections = null, int selectedAtomsCount = 0)
        {
            //If there are no atom selections, then the client is just performing old-school single atom interactions.
            if (atomSelections == null)
            {
                for (int i = 0; i < count; i++)
                {
                    Interactions.Add(new Interaction(data[i].Position, data[i].PlayerID, data[i].InputID, (InteractionType)data[i].InteractionType, data[i].GradientScaleFactor));
                }
                return;
            }
            //Count how many interactions are in the selectedAtoms data. if it doesn't match the number of interactions, then don't do anything this frame.
            //Hopefully, on the next frame we'll have received the full details.
            //TODO robustify this.
            int numSelections = 0;
            for (int i = 0; i < selectedAtomsCount; i++)
            {
                if (atomSelections[i] == -1)
                    numSelections++;
            }
            if (numSelections != count)
                return;

            int selectionIndex = 0;
            for (int i = 0; i < count; i++)
            {
                var newInteraction = new Interaction(data[i].Position, data[i].PlayerID, data[i].InputID,
                    (InteractionType)data[i].InteractionType, data[i].GradientScaleFactor, new EquatableSet());
                //Loop through the selected atoms stream and populate any selected atoms.
                while (selectionIndex < selectedAtomsCount)
                {
                    if (atomSelections[selectionIndex] == -1)
                    {
                        selectionIndex++;
                        break;
                    }
                    else
                        newInteraction.SelectedAtoms.Add(atomSelections[selectionIndex]);
                    selectionIndex++;
                }
                Interactions.Add(newInteraction);
            }
        }

        /// <summary>
        /// Copies the locations from the specified array.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="count">The count.</param>
        public void CopyLocationsFrom(Vector3[] data, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Interactions.Add(new Interaction(data[i]));
            }
        }
    }
}