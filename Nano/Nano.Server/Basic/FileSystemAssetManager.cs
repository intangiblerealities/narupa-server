﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using Nano.Loading;
using Rug.Cmd;

namespace Nano.Server.Basic
{
    [XmlName("FileSystemAssetManager")] 
    public class FileSystemAssetManager : IAssetManager
    {
        public string Folder { get; set; }         

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(Folder), Folder); 

            return element; 
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            Folder = Helper.GetAttributeValue(node, nameof(Folder), "^/Assets"); 
        }

        /// <inheritdoc />
        public void RegisterArguments(ArgumentParser parser)
        {

        }

        /// <inheritdoc />
        public void ValidateArguments(IReporter reporter)
        {
            reporter.PrintDetail(Direction.Action, "assets folder", Helper.ResolvePath(Folder)); 
        }

        /// <inheritdoc />
        public async Task<Stream> GetAsset(Uri assetUri, IReporter reporter = null)
        {

            //First, assume asset path is a direct path.
            var file = GetAssetPath(assetUri, reporter);
            if (file.Exists == false)
            {
                file = GetAssetFromRelativeUri(assetUri, reporter);
            }
            
            if (file.Exists == false)
            {
                throw new FileNotFoundException($"Asset file could not be found. {assetUri}"); 
            }

            return file.OpenRead(); 
        }

        private FileInfo GetAssetFromRelativeUri(Uri assetUri, IReporter reporter = null)
        {
            return new FileInfo(Path.Combine(Helper.ResolvePath(Folder), assetUri.OriginalString.TrimStart('/', '\\')));
        }
        
        private FileInfo GetAssetPath(Uri assetUri, IReporter reporter = null)
        {
            var file = new FileInfo(Helper.ResolvePath(assetUri.OriginalString));
            return file;
        }
        /// <inheritdoc />
        public async Task<bool> AssetExists(Uri assetUri, IReporter reporter = null)
        {
            try
            {

                var file = GetAssetPath(assetUri, reporter);
                if (file.Exists) return true;

                file = GetAssetFromRelativeUri(assetUri, reporter);
                return file.Exists;
            }
            catch
            {
                return false; 
            }
        }
    }
}