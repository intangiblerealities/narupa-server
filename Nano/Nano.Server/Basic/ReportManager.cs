﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Xml;
using Nano.Loading;
using Rug.Cmd;

namespace Nano.Server.Basic
{
    [XmlName("ReportManager")]
    public class ReportManager : IReportManager
    {
        private readonly StringArgument folderArgument = new StringArgument("Logging Folder", "Full or relitive path to logging folder", "Full or relitive path to logging folder");
        private string originalFolder = "^/logs";
        private IReporter reporter;
        private readonly ConcurrentDictionary<Uri, IReporter> reporters = new ConcurrentDictionary<Uri, IReporter>();

        public string Folder { get; private set; }

        private ReportOptions[] options;

        public ReportManager(ReportVerbosity verbosity = ReportVerbosity.Debug)
        {
            options = new ReportOptions[Enum.GetValues(typeof(ReportContext)).Length];  

            for (int contextIndex = 0; contextIndex < options.Length; contextIndex++)
            {
                options[contextIndex] = new ReportOptions
                {
                    Folder = null, 
                    Context = (ReportContext)contextIndex,
                    Mode = LogMode.Console, 
                    Verbosity = verbosity
                };
            }
            
            string folder = Helper.ResolvePath(originalFolder);

            Path.GetFullPath(folder);

            Folder = folder;

            DirectoryInfo folderInfo = new DirectoryInfo(Folder);

            if (folderInfo.Exists == false)
            {
                folderInfo.Create();
            }

//            options[(int) ReportContext.None].Mode = LogMode.Console;
//            options[(int) ReportContext.Discovery].Mode = LogMode.Console; 
//            options[(int) ReportContext.Load].Mode = LogMode.ConsoleAndFile;
//            options[(int) ReportContext.Monitor].Mode = LogMode.Console;
//            options[(int) ReportContext.Report].Mode = LogMode.Console;
//            options[(int) ReportContext.Service].Mode = LogMode.ConsoleAndFile;
//            options[(int) ReportContext.Session].Mode = LogMode.File;
        }
        
        #region Arguments / Loading / Initialize

        /// <inheritdoc />
        public void RegisterArguments(ArgumentParser parser)
        {
            folderArgument.Reset();

            parser.Add("-", "log-folder", folderArgument);
        }

        /// <inheritdoc />
        public void ValidateArguments(IReporter reporter)
        {
            string folder = Helper.ResolvePath(folderArgument.Defined == true ? folderArgument.Value : originalFolder);

            Path.GetFullPath(folder);

            Folder = folder;

            DirectoryInfo folderInfo = new DirectoryInfo(Folder);

            if (folderInfo.Exists == false)
            {
                folderInfo.Create();
            }

            reporter.PrintEmphasized($"Logging to: {Folder}");

            foreach (ReportOptions option in options)
            {
                if (string.IsNullOrEmpty(option.Folder) == true)
                {
                    continue; 
                }

                string optionFolder = Path.Combine(Folder, option.Folder); 

                Path.GetFullPath(optionFolder);

                DirectoryInfo optionFolderInfo = new DirectoryInfo(optionFolder);

                if (optionFolderInfo.Exists == false)
                {
                    optionFolderInfo.Create();
                }

                reporter.PrintDebug($"Logging {option.Context} to: {optionFolder}");
            }
        }
        
        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            originalFolder = Helper.GetAttributeValue(node, nameof(Folder), "^/logs");

            foreach (ReportOptions logContextOptions in Loader.LoadObjects<ReportOptions>(context, node))
            {
                options[(int)logContextOptions.Context] = logContextOptions; 
            }
        }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(Folder), originalFolder);

            Loader.SaveObjects<ReportOptions>(context, element, options); 
            
            return element;
        }

        /// <inheritdoc />
        public void Initialize(IServer serverContext, IReporter reporter)
        {
            this.reporter = reporter;
        }
        
        #endregion
       
        /// <inheritdoc />
        public IReporter BeginReport(ReportContext context, string name, out Uri logIdentifier)
        {
            ReportOptions option = options[(int) context];

            string logPath; 
            
            if (string.IsNullOrEmpty(option.Folder) == true)
            {
                logPath = Path.Combine(Folder, $"{DateTime.UtcNow:yyyyMMdd-HHmmss-fff}-{name}.log");
            }
            else
            {
                logPath = Path.Combine(Folder, option.Folder, $"{DateTime.UtcNow:yyyyMMdd-HHmmss-fff}-{name}.log");
            }

            IReporter contextReporter;
            
            switch (option.Mode)
            {
                case LogMode.Console:
                    contextReporter = reporter; 
                    break;
                case LogMode.File:
                    contextReporter = new FileReporter(logPath, null) {ReportVerbosity = option.Verbosity};
                    this.reporter.PrintNormal($"Open {context} {name} log: {logPath}");
                    break;
                case LogMode.ConsoleAndFile:
                    contextReporter = new FileReporter(logPath, reporter) {ReportVerbosity = option.Verbosity};
                    this.reporter.PrintNormal($"Open {context} {name} log: {logPath}");
                    break;
                case LogMode.DontLog:
                    contextReporter = new NullReporter(); 
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            logIdentifier = new Uri(logPath, UriKind.RelativeOrAbsolute); 

            reporters[logIdentifier] = contextReporter;

            return contextReporter;
        }

        /// <inheritdoc />
        public void EndReport(Uri identifier)
        {
            if (reporters.TryRemove(identifier, out IReporter removedReporter) == false)
            {
                return;
            }

            (removedReporter as FileReporter)?.Flush(); 
            (removedReporter as IDisposable)?.Dispose();
        }
    }

    public enum LogMode
    {           
        Console,
        File,
        ConsoleAndFile, 
        DontLog,
    }

    [XmlName("ReportOptions")]
    public class ReportOptions : ILoadable
    {
        public ReportContext Context { get; set; }
        public LogMode Mode { get; set; }
        public ReportVerbosity Verbosity { get; set; }
        public string Folder { get; set; } 

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(Context), Context);
            Helper.AppendAttributeAndValue(element, nameof(Mode), Mode);
            Helper.AppendAttributeAndValue(element, nameof(Verbosity), Verbosity);
            Helper.AppendAttributeAndValue(element, nameof(Folder), Folder);
            
            return element;
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            Context = Helper.GetAttributeValue(node, nameof(Context), Context);
            Mode = Helper.GetAttributeValue(node, nameof(Mode), Mode);
            Verbosity = Helper.GetAttributeValue(node, nameof(Verbosity), Verbosity);
            Folder = Helper.GetAttributeValue(node, nameof(Folder), Folder);            
        }
    }
}