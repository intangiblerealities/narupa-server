﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Nano.Loading;
using Nano.Transport.Comms.Security;
using Rug.Cmd;

namespace Nano.Server.Basic
{
    [XmlName("ReservationSecurityProvider")] 
    public class ReservationSecurityProvider : ISecurityProvider, IDisposable
    {
        private readonly StringArgument timeoutArgument = new StringArgument("Timeout", "Reservation timeout", "Reservation timeout");
        private readonly StringArgument sessionTimeArgument = new StringArgument("SessionTime", "Reserved session time", "Reserved session time");

        private IReporter reporter;
        private IServer serverContext;
        private Uri reportIdentifier;

        public TimeSpan Timeout { get; set; }

        public TimeSpan? SessionTime { get; set; }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(Timeout), Timeout.TotalSeconds);

            if (SessionTime.HasValue == true)
            {
                Helper.AppendAttributeAndValue(element, nameof(SessionTime), SessionTime.Value.TotalSeconds);
            }

            return element; 
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            Timeout = TimeSpan.FromSeconds(Helper.GetAttributeValue(node, nameof(Timeout), 10));

            double sessionTime = Helper.GetAttributeValue(node, nameof(SessionTime), 0);
            SessionTime = sessionTime <= 0 ? null : (TimeSpan?)TimeSpan.FromSeconds(sessionTime);
        }

        /// <inheritdoc />
        public void RegisterArguments(ArgumentParser parser)
        {
            parser.Add("-", "reservation-timeout", timeoutArgument);
            parser.Add("-", "reserved-session-time", sessionTimeArgument);            
        }

        /// <inheritdoc />
        public void ValidateArguments(IReporter reporter)
        {
            if (timeoutArgument.Defined == true)
            {
                if (double.TryParse(timeoutArgument.Value, out double timeoutValue) == false)
                {
                    throw new ArgumentException($"Invalid reservation-timeout {timeoutArgument.Value}", nameof(timeoutArgument));
                }

                Timeout = TimeSpan.FromSeconds(timeoutValue);
            }

            if (sessionTimeArgument.Defined == true)
            {
                if (double.TryParse(sessionTimeArgument.Value, out double sessionTimeValue) == false)
                {
                    throw new ArgumentException($"Invalid reserved-session-time {sessionTimeArgument.Value}", nameof(sessionTimeArgument));
                }

                SessionTime = sessionTimeValue <= 0 ? null : (TimeSpan?)TimeSpan.FromSeconds(sessionTimeValue);
            }
        }

        /// <inheritdoc />
        public ITransportSecurityProvider TransportSecurityProvider { get; private set; }

        /// <inheritdoc />
        public void Initialize(IServer serverContext, IReporter reporter)
        {
            this.serverContext = serverContext;
            this.reporter = serverContext.ReportManager.BeginReport(ReportContext.Security, "reservation-security-provider", out reportIdentifier);
            
            TransportSecurityProvider = new Nano.Transport.Comms.Security.ReservationSecurityProvider(Timeout, SessionTime);

            TransportSecurityProvider.LoadSimulation += path => this.reporter.PrintNormal($"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff} Load: {path}");
            TransportSecurityProvider.ReservationMade += () => this.reporter.PrintNormal($"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff} Reservation made");
            TransportSecurityProvider.ReservationExpired += () => this.reporter.PrintNormal($"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff} Reservation expired");
            TransportSecurityProvider.ReservationReleased += () => this.reporter.PrintNormal($"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff} Reservation released");
        }

        /// <inheritdoc />
        public async Task Start(IServer serverContext, CancellationToken cancel)
        {
            try
            {
                reporter.PrintNormal(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "reservation-security-provider started");

                do
                {
                    await Task.Delay(60000, cancel); 
                }
                while (cancel.IsCancellationRequested == false); 
            }
            catch (TaskCanceledException _)
            {
                reporter.PrintDebug(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "reservation-security-provider task canceled");
            }
            finally
            {
                reporter.PrintNormal(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "reservation-security-provider ended");
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            serverContext.ReportManager.EndReport(reportIdentifier);
        }
    }
}