﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using SlimMath;

namespace Nano.Science.Simulation.Tests
{
    [TestFixture()]
    public class InteractionListTests
    {
        [Test()]
        public void SetInteractionsTestKeepOne()
        {
            InteractionList list = new InteractionList();
            Interaction a = new Interaction(Vector3.Zero);
            a.SelectedAtoms.Add(1);
            list.Add(a);

            Interaction b = new Interaction(Vector3.One * 0.5f);
            List<Interaction> interactions = new List<Interaction>();
            interactions.Add(b);
            list.SetInteractions(interactions);

            Assert.True(list.Count == 1);
        }

        [Test()]
        public void SetInteractionsTestPreserveSelectedAtom()
        {
            InteractionList list = new InteractionList();
            Interaction a = new Interaction(Vector3.Zero);
            a.SelectedAtoms.Add(1);
            list.Add(a);

            Interaction b = new Interaction(Vector3.One * 0.5f);
            List<Interaction> interactions = new List<Interaction>();
            interactions.Add(b);
            list.SetInteractions(interactions);

            Assert.True(list[0].SelectedAtoms.First() == 1);
        }

        [Test()]
        public void SetInteractionsTestDistanceCutoffSelectedAtom()
        {
            InteractionList list = new InteractionList();
            Interaction a = new Interaction(Vector3.Zero);
            a.SelectedAtoms.Add(1);
            list.Add(a);

            Interaction b = new Interaction(Vector3.One * 1.1f);
            List<Interaction> interactions = new List<Interaction>();
            interactions.Add(b);
            list.SetInteractions(interactions);

            Assert.True(list[0].SelectedAtoms.Count == 0);
        }

        [Test()]
        public void SetInteractionsTestDistanceCutoffPosition()
        {
            InteractionList list = new InteractionList();
            Interaction a = new Interaction(Vector3.Zero);
            a.SelectedAtoms.Add(1);
            list.Add(a);

            Interaction b = new Interaction(Vector3.One * 1.1f);
            List<Interaction> interactions = new List<Interaction>();
            interactions.Add(b);
            list.SetInteractions(interactions);
            Assert.AreEqual((list[0].Position - b.Position).Length, 0.0, 0.000001);
        }

        [Test()]
        public void SetInteractionsTestDifferentPlayers()
        {
            InteractionList list = new InteractionList();
            Interaction a = new Interaction(Vector3.Zero);
            a.SelectedAtoms.Add(1);
            a.PlayerId = 1;
            list.Add(a);

            Interaction b = new Interaction(Vector3.One * 0.5f);
            List<Interaction> interactions = new List<Interaction>();
            interactions.Add(b);
            list.SetInteractions(interactions);

            Assert.True(list.Count == 1);
            Assert.True(list[0].PlayerId == 0);
        }

        [Test()]
        public void SetInteractionsTestDifferentInputs()
        {
            InteractionList list = new InteractionList();
            Interaction a = new Interaction(Vector3.Zero);
            a.SelectedAtoms.Add(1);
            a.InputId = 1;
            list.Add(a);

            Interaction b = new Interaction(Vector3.One * 0.5f);
            List<Interaction> interactions = new List<Interaction>();
            interactions.Add(b);
            list.SetInteractions(interactions);

            Assert.True(list.Count == 1);
            Assert.True(list[0].InputId == 0);
        }

        [Test()]
        public void SetInteractionsTestRemoveRandomInteraction()
        {
            InteractionList interactionList = new InteractionList();
            int numPlayers = 4;
            Random random = new Random(123);
            List<Interaction> list = new List<Interaction>();
            for (ushort i = 0; i < numPlayers; i++)
            {
                for (ushort jj = 0; jj < 2; jj++)
                {
                    Interaction a = new Interaction(Vector3.Zero);
                    a.SelectedAtoms.Add(1);
                    a.PlayerId = i;
                    a.InputId = jj;
                    a.Position = new Vector3(10 * (float)random.NextDouble(), 10 * (float)random.NextDouble(), 10 * (float)random.NextDouble());
                    list.Add(a);
                }
            }
            interactionList.SetInteractions(list);

            int toRemove = random.Next(numPlayers * 2);
            list.RemoveAt(toRemove);
            interactionList.SetInteractions(list);

            Assert.IsTrue(interactionList.Count == list.Count);

            foreach (Interaction interaction in interactionList)
            {
                bool foundMatch = false;
                foreach (Interaction b in list)
                {
                    if (interaction.Equals(b))
                    {
                        foundMatch = true;
                        break;
                    }
                }
                if (foundMatch == false) Assert.Fail();
            }
        }
    }
}