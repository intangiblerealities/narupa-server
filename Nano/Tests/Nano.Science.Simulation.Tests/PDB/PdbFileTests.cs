﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NUnit.Framework;
using System.Collections.Generic;
using Narupa.Trajectory.Load.PDB;

namespace Nano.Science.Simulation.Tests.PDB
{
    [TestFixture()]
    public class PdbFileTests
    {
        private TestHelper helper; 
        [SetUp]
        public void Initialize()
        {
            helper = new TestHelper();
        }

        [Test()]
        public void TestPdbBonds()
        {
            //using 2ala_traj_single.pdb.
            PdbFile file = new PdbFile("^/Assets/2ala.pdb", null);

            List<PdbBond> testBonds = new List<PdbBond>();
            PdbModel model = file.Models[0];

            testBonds.Add(new PdbBond(model.AtomsByNumber[1], model.AtomsByNumber[2]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[2], model.AtomsByNumber[3]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[2], model.AtomsByNumber[5]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[2], model.AtomsByNumber[4]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[5], model.AtomsByNumber[6]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[5], model.AtomsByNumber[7]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[7], model.AtomsByNumber[8]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[7], model.AtomsByNumber[9]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[9], model.AtomsByNumber[10]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[9], model.AtomsByNumber[11]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[9], model.AtomsByNumber[15]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[11], model.AtomsByNumber[13]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[11], model.AtomsByNumber[14]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[11], model.AtomsByNumber[12]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[15], model.AtomsByNumber[16]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[15], model.AtomsByNumber[17]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[17], model.AtomsByNumber[18]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[17], model.AtomsByNumber[19]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[19], model.AtomsByNumber[21]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[19], model.AtomsByNumber[20]));
            testBonds.Add(new PdbBond(model.AtomsByNumber[19], model.AtomsByNumber[22]));

            int result1 = testBonds.Count;
            int result2 = file.Bonds.Count;
            bool result3 = false;
            for (int testBondNumber = 0; testBondNumber < testBonds.Count; testBondNumber++)
            {
                if (file.Bonds.Contains(testBonds[testBondNumber]))
                {
                    result3 = true;
                }
            }

            Assert.AreEqual(21, result1);
            Assert.AreEqual(21, result2);
            Assert.AreEqual(true, result3);
        }
    }
}