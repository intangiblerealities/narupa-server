﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using NUnit.Framework;
using System.Collections.Generic;

namespace Nano.Science.Tests
{
    [TestFixture]
    public class AtomSelectionHelperTests
    {
        private TestHelper helper;

        [SetUp]
        public void Initialize()
        {
            helper = new TestHelper();
        }

        [Test]
        public void GetParentAddressTest()
        {
            string address = "/CO2/0/0";
            string parentAddress = AtomSelectionHelper.GetParentAddress(address);
            Assert.AreEqual("/CO2/0", parentAddress);

            parentAddress = AtomSelectionHelper.GetParentAddress(parentAddress);
            Assert.AreEqual("/CO2", parentAddress);

            parentAddress = AtomSelectionHelper.GetParentAddress(parentAddress);
            Assert.AreEqual("", parentAddress);
        }

    }
}