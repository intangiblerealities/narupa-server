﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Variables;
using NUnit.Framework;

namespace Nano.Tests.Variables
{
    [TestFixture()]
    public class VariableCollectionTests
    {
        private VariableCollection CreateCollection()
        {
            VariableCollection collection = new VariableCollection
            {
                { VariableName.Density, VariableType.Float },
                { VariableName.NumberOfParticles, VariableType.UInt32 }
            };

            return collection;
        }

        [Test]
        public void CreateTest()
        {
            CreateCollection();
        }

        [Test]
        public void ContainsTest()
        {
            VariableCollection collection = CreateCollection();

            if (collection.ContainsKey(VariableName.Density) == false)
            {
                Assert.Fail();
            }

            if (collection.ContainsKey(VariableName.NumberOfParticles) == false)
            {
                Assert.Fail();
            }
        }
    }
}