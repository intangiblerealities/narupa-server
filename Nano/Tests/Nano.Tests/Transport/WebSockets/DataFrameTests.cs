﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using Nano.WebSockets;
using NUnit.Framework;

namespace Nano.Tests.Transport.WebSockets
{
    [TestFixture]
    public class DataFrameTests
    {
        [Test()]
        public void ClearMaskTest()
        {
            DataFrame dataFrame = new DataFrame();

            dataFrame.CreateMask();

            dataFrame.ClearMask();

            Assert.AreEqual(dataFrame.HasMask, false);
            Assert.AreEqual(dataFrame.Mask.Length, 4);

            int sum = 0;
            for (int i = 0; i < dataFrame.Mask.Length; i++)
            {
                sum += dataFrame.Mask[i];
            }

            Assert.AreEqual(sum, 0);
        }

        [Test()]
        public void CreateMaskTest()
        {
            DataFrame dataFrame = new DataFrame();

            dataFrame.CreateMask();

            Assert.AreEqual(dataFrame.HasMask, true);
            Assert.AreEqual(dataFrame.Mask.Length, 4);

            int sum = 0;
            for (int i = 0; i < dataFrame.Mask.Length; i++)
            {
                sum += dataFrame.Mask[i];
            }

            Assert.AreEqual(sum > 0, true);
        }

        [Test()]
        public void LoopThroughTest0_Mask()
        {
            LoopThroughTest(0, true);
        }

        [Test()]
        public void LoopThroughTest0_NoMask()
        {
            LoopThroughTest(0, false);
        }

        [Test()]
        public void LoopThroughTest125_Mask()
        {
            LoopThroughTest(125, true);
        }

        [Test()]
        public void LoopThroughTest125_NoMask()
        {
            LoopThroughTest(125, false);
        }

        [Test()]
        public void LoopThroughTest256_Mask()
        {
            LoopThroughTest(256, true);
        }

        [Test()]
        public void LoopThroughTest256_NoMask()
        {
            LoopThroughTest(256, false);
        }

        [Test()]
        public void LoopThroughTest75535_Mask()
        {
            LoopThroughTest(75535, true);
        }

        [Test()]
        public void LoopThroughTest75535_NoMask()
        {
            LoopThroughTest(75535, false);
        }

        private static void CompareBytes(byte[] payloadData1, byte[] payloadData2)
        {
            for (int i = 0; i < payloadData1.Length; i++)
            {
                Assert.AreEqual(payloadData1[i], payloadData2[i], $@"Failed at index {i}");
            }
        }

        private static void LoopThroughTest(int count, bool mask)
        {
            Random rand = new Random();

            byte[] originalData = new byte[count];

            rand.NextBytes(originalData);

            DataFrame frame1 = new DataFrame();
            frame1.ClearMask();

            if (mask == true)
            {
                frame1.CreateMask();
            }

            frame1.PayloadData = (byte[])originalData.Clone();
            frame1.PayloadLength = originalData.Length;
            frame1.Opcode = Opcode.Binary;

            DataFrame frame2 = new DataFrame();

            using (MemoryStream stream = new MemoryStream())
            using (BinaryWriter writer = new BinaryWriter(stream))
            using (BinaryReader reader = new BinaryReader(stream))
            {
                frame1.Write(writer);

                stream.Position = 0;

                frame2.Read(reader);
            }

            Assert.AreEqual(frame1.HasMask, frame2.HasMask);
            Assert.AreEqual(frame1.Opcode, frame2.Opcode);
            Assert.AreEqual(frame1.PayloadData.Length, frame2.PayloadData.Length);
            Assert.AreEqual(frame1.PayloadLength, frame2.PayloadLength);

            CompareBytes(originalData, frame2.PayloadData);
        }
    }
}