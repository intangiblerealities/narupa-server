﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.WebSockets;
using NUnit.Framework;

namespace Nano.Tests.Transport.WebSockets
{
    [TestFixture()]
    public class KeysTests
    {
        [Test()]
        public void GetAcceptStringTest()
        {
            string clientKey = Keys.CreateClientKey();

            string acceptString1 = Keys.GetAcceptString(clientKey);

            string acceptString2 = Keys.GetAcceptString(clientKey);

            Assert.AreEqual(acceptString1, acceptString2); 
        }

        [Test()]
        public void CreateClientKeyTest()
        {
            string clientKey = Keys.CreateClientKey();

            Assert.True(clientKey.Length > 0, "Client key has no length");
            Assert.True(clientKey.Trim().Length > 0, "Client key is empty");
        }
    }
}