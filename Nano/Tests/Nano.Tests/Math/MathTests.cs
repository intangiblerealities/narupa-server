using NUnit.Framework;
using SlimMath;

namespace Nano.Tests.Math
{
    [TestFixture]
    public class MathTests
    {
        [Test]
        public void TestIdentityQuaternionFromEuler()
        {
            Vector3 rotation = Vector3.Zero;
            Quaternion quat = MathUtilities.QuaternionFromEuler(rotation, radians: false);
            Assert.AreEqual(Quaternion.Identity, quat);
        }

        private bool QuaternionEqual(Quaternion a, Quaternion b, float tolerance = 0.0001f)
        {
            Vector4 vecA = new Vector4(a.X, a.Y, a.Z, a.W);
            Vector4 vecB = new Vector4(b.X, b.Y, b.Z ,b.W);
            return (Vector4.Distance(vecA, vecB) < tolerance);
        }
        [Test]
        public void TestQuaternionFromEuler()
        {
            //x
            Vector3 rotation = new Vector3(90,0,0);
            Quaternion quat = MathUtilities.QuaternionFromEuler(rotation, radians: false);
            Quaternion reference = new Quaternion(0.7071f, 0f, 0f, 0.7071f);
            Assert.IsTrue(QuaternionEqual(quat, reference), $"Expected {reference}, was: {quat}");
            
            //y
            rotation = new Vector3(0,90,0);
            quat = MathUtilities.QuaternionFromEuler(rotation, radians: false);
            reference = new Quaternion(0f, 0.7071f, 0f, 0.7071f);
            Assert.IsTrue(QuaternionEqual(quat, reference), $"Expected {reference}, was: {quat}");
            
            //z
            rotation = new Vector3(0,0,90);
            quat = MathUtilities.QuaternionFromEuler(rotation, radians: false);
            reference = new Quaternion(0f, 0f, 0.7071f, 0.7071f);
            Assert.IsTrue(QuaternionEqual(quat, reference), $"Expected {reference}, was: {quat}");
        }
        
        [Test]
        public void TestEulerFromIdentityQuaternion()
        {
            Vector3 rotation = Vector3.Zero;
            Quaternion quat = MathUtilities.QuaternionFromEuler(rotation, radians: false);
            Vector3 newRotation = MathUtilities.EulerFromQuaternion(quat);
            Assert.AreEqual(Vector3.Distance(newRotation, rotation), 0f, 0.0001);
        }
        
        [Test]
        public void TestEulerFromQuaternion()
        {
            //x
            Vector3 rotation = new Vector3(90,0,0);
            Quaternion reference = new Quaternion(0.7071f, 0f, 0f, 0.7071f);
            Vector3 newRotation = MathUtilities.EulerFromQuaternion(reference, radians: false);
            Assert.AreEqual(0f, Vector3.Distance(newRotation, rotation), 0.01, $"Expected {rotation}, was: {newRotation}");
 
        }
        
    }
}