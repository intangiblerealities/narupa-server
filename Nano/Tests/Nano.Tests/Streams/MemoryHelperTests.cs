﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Runtime.InteropServices;
using Nano.Transport.Streams;
using NUnit.Framework;

namespace Nano.Tests.Streams
{

    [TestFixture()]
    public class MemoryHelperTests
    {
        [StructLayout(LayoutKind.Sequential)]
        struct TestStruct
        {
            public float X, Y, Z, W;
            
            public override bool Equals(object obj)
            {
                if (obj == null || GetType() != obj.GetType())
                {
                    return false;
                }

                TestStruct other = (TestStruct)obj;

                return
                    X == other.X &&
                    Y == other.Y &&
                    Z == other.Z &&
                    W == other.W;
            }


        }


        [Test]
        public void GetBytesTest_Int()
        {
            int initialValue = unchecked((int)0xF785C42A);

            int index = 0;
            byte[] bytes = MemoryHelper.GetBytes(initialValue, ref index);

            int index2 = 0;
            int resultValue = MemoryHelper.FromBytes<int>(bytes, ref index2);

            Assert.AreEqual(index, index2);
            Assert.AreEqual(initialValue, resultValue);
        }

        [Test]
        public void GetBytesTest_TestStruct()
        {
            TestStruct initialValue = new TestStruct() { X = 3, Y = 6, Z = 12, W = 24 };

            int index = 0;
            byte[] bytes = MemoryHelper.GetBytes(initialValue, ref index);

            int index2 = 0;
            TestStruct resultValue = MemoryHelper.FromBytes<TestStruct>(bytes, ref index2);

            Assert.AreEqual(index, index2);
            Assert.AreEqual(initialValue, resultValue);
        }


        private void GetBytesTest_T_Array<T>(T[] initialValue) where T : struct
        {
            int index = 0;
            byte[] bytes = MemoryHelper.GetArrayBytes(initialValue, ref index);

            T[] resultValue = MemoryHelper.ArrayFromBytes<T>(bytes, initialValue.Length);

            Assert.AreEqual(initialValue.Length, resultValue.Length);

            for (int i = 0; i < initialValue.Length; i++)
            {
                Assert.AreEqual(initialValue[i], resultValue[i]);
            }
        }

        private void GetBytesTest_T_Array<T>(T[] initialValue, int valueCount, int offset) where T : struct
        {

            byte[] buffer = new byte[1024];

            int index = 0;

            for (int i = 0; i < offset; i++)
            {
                buffer[index++] = 0xFF;
            }

            int startIndex = index;

            MemoryHelper.GetArrayBytes(initialValue, valueCount, buffer, ref index);

            int readIndex = startIndex; 
            T[] resultValue = MemoryHelper.ArrayFromBytes<T>(buffer, ref readIndex, valueCount);

            Assert.AreEqual(valueCount, resultValue.Length);

            for (int i = 0; i < valueCount; i++)
            {
                Assert.AreEqual(initialValue[i], resultValue[i]);
            }
        }

        [Test]
        public void GetBytesTest_Int_Array()
        {
            //int[] initialValue = new int[] { unchecked((int)0x11223344), unchecked((int)0x55667788), unchecked((int)0x99AABBCC), unchecked((int)0xDDEEFF00) };
            int[] initialValue = new int[] { unchecked(1), unchecked(2), unchecked(3), unchecked(4) };

            GetBytesTest_T_Array(initialValue);
        }

        [Test]
        public void GetBytesTest_Int_Array2()
        {
            //int[] initialValue = new int[] { unchecked((int)0x11223344), unchecked((int)0x55667788), unchecked((int)0x99AABBCC), unchecked((int)0xDDEEFF00) };
            int[] initialValue = new int[] { unchecked(1), unchecked(2), unchecked(3), unchecked(4) };

            GetBytesTest_T_Array(initialValue, initialValue.Length, 24);
        }


        [Test]
        public void GetBytesTest_Int_Array3()
        {
            //int[] initialValue = new int[] { unchecked((int)0x11223344), unchecked((int)0x55667788), unchecked((int)0x99AABBCC), unchecked((int)0xDDEEFF00) };
            int[] initialValue = new int[] { unchecked(1), unchecked(2), unchecked(3), unchecked(4), unchecked(5), unchecked(6), unchecked(7), unchecked(8) };

            GetBytesTest_T_Array(initialValue, 5, 24);
        }

        [Test]
        public void GetBytesTest_TestStruct_Array()
        {
            TestStruct[] initialValue = new TestStruct[] 
            {
                new TestStruct() { X = 1, Y = 2, Z = 3, W = 4 },
                new TestStruct() { X = 5, Y = 6, Z = 7, W = 8 },
                new TestStruct() { X = 9, Y = 10, Z = 11, W = 12 },
                new TestStruct() { X = 13, Y = 14, Z = 15, W = 15 },
            };

            GetBytesTest_T_Array(initialValue);
        }

        [Test]
        public void GetBytesTest_TestStruct_Array2()
        {
            TestStruct[] initialValue = new TestStruct[]
            {
                new TestStruct() { X = 1, Y = 2, Z = 3, W = 4 },
                new TestStruct() { X = 5, Y = 6, Z = 7, W = 8 },
                new TestStruct() { X = 9, Y = 10, Z = 11, W = 12 },
                new TestStruct() { X = 13, Y = 14, Z = 15, W = 15 },
            };

            GetBytesTest_T_Array(initialValue, initialValue.Length, 10);
        }

        [Test]
        public void GetBytesTest_TestStruct_Array3()
        {
            TestStruct[] initialValue = new TestStruct[]
            {
                new TestStruct() { X = 1, Y = 2, Z = 3, W = 4 },
                new TestStruct() { X = 5, Y = 6, Z = 7, W = 8 },
                new TestStruct() { X = 9, Y = 10, Z = 11, W = 12 },
                new TestStruct() { X = 13, Y = 14, Z = 15, W = 16 },
                new TestStruct() { X = 17, Y = 18, Z = 19, W = 20 },
                new TestStruct() { X = 21, Y = 22, Z = 23, W = 24 },
                new TestStruct() { X = 25, Y = 26, Z = 27, W = 28 },
            };

            GetBytesTest_T_Array(initialValue, 5, 10);
        }
    }
}