﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Nano.Rainbow.v1_0
{
    public class Organization
    {
        public static readonly string MethodPath = "/1.0/organization";

        public Guid ID { get; set; }

        public string Name { get; set; }

        public string SessionToken { get; set; }

        public string GetPath()
        {
            return $@"{ID.ToString().ToLowerInvariant().Substring(0, 4)}-{PathHelper.SanitiseFileName(Name)}";
        }
    }
}