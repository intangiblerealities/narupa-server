﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

namespace Nano.Rainbow.v1_0
{
    public class ReservationRequest
    {
        public static readonly string MethodPath = "/1.0/reserve";

        public string AccessToken { get; set; }

        public string SimulationPath { get; set; }
    }
}