﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;

namespace Nano.Rainbow.v1_0
{
    public class SimulationMetadata
    {
        public static readonly string MethodPath = "/1.0/simulation";

        public string Formula { get; set; }

        public bool HasLive { get; set; }

        public bool HasRecording { get; set; }

        public DateTime LastModified { get; set; }

        public string Name { get; set; }

        public string Species { get; set; }

        public List<string> Tags { get; set; }

        public Uri Uri { get; set; }

        public List<string> Variables { get; set; }
    }
}