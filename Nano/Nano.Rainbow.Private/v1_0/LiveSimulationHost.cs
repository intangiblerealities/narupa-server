﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the ^GPL. See License.txt in the project root for license information.

using System;

namespace Nano.Rainbow.Private.v1_0
{
    public class LiveSimulationHost
    {
        public static readonly string MethodPath = "/1.0/server";

        public Uri Host { get; set; }

        public string Name { get; set; }

        public Uri ReservationHost { get; set; }

        public ConnectionSecurityType SecurityType { get; set; }

        public DateTime Started { get; set; }

        public string Version { get; set; }
    }
}