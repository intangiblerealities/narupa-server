﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the ^GPL. See License.txt in the project root for license information.

namespace Nano.Rainbow.Private.v1_0
{
    public enum ConnectionSessionState
    {
        Unreserved = 0,
        Reserved = 1,
        SessionActive = 2,
        Stopped = 3
    }
}