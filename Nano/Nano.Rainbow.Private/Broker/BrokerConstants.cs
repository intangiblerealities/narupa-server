﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Nano.WebSockets;

namespace Nano.Rainbow.Private.Broker
{
    public static class BrokerConstants
    {
        public static readonly string ApiAuthorizationKey = "22A2C8C0-482B-4FF5-940E-0D6263A0B81C";
        public static readonly Guid BrokerKey = new Guid("43109491-5D69-4C5E-BD67-2288C7F28E9B");

        public static string GenerateApiAuthorizationToken()
        {
            return Keys.ComputeSecretString(ApiAuthorizationKey, BrokerKey);
        }

        public static string GenerateAuthorizationToken(string serverUrl)
        {
            return Keys.ComputeSecretString(serverUrl.ToLowerInvariant(), BrokerKey);
        }

        public static bool ValidateApiAuthorizationToken(string token)
        {
            return Keys.ComputeSecretString(ApiAuthorizationKey, BrokerKey)
                .Equals(token);
        }

        public static bool ValidateToken(string serverUrl, string token)
        {
            return Keys.ComputeSecretString(serverUrl.ToLowerInvariant(), BrokerKey)
                .Equals(token);
        }
    }
}