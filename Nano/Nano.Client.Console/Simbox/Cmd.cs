﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Rug.Cmd;
using Rug.Osc;
using System;

namespace Nano.Client.Console.Simbox
{
    internal static class Cmd
    {
        private static readonly object SyncLock = new object();

        public static bool ShowFullErrors { get; set; }

        static Cmd()
        {
            ShowFullErrors = true;
        }

        public static string GetMemStringFromBytes(long bytes, bool space)
        {
            decimal num = bytes / 1024M;
            string str = "KB";
            while (num >= 1000M)
            {
                if (str == "KB")
                {
                    num /= 1024M;
                    str = "MB";
                }
                else
                {
                    if (str == "MB")
                    {
                        num /= 1024M;
                        str = "GB";
                        continue;
                    }
                    if (str == "GB")
                    {
                        num /= 1024M;
                        str = "TB";
                        continue;
                    }
                    if (str == "TB")
                    {
                        num /= 1024M;
                        str = "PB";
                        continue;
                    }
                    if (str == "PB")
                    {
                        num /= 1024M;
                        str = "XB";
                        continue;
                    }
                    if (str == "XB")
                    {
                        num /= 1024M;
                        str = "ZB";
                        continue;
                    }
                    if (str == "ZB")
                    {
                        num /= 1024M;
                        str = "YB";
                        continue;
                    }
                    if (str == "YB")
                    {
                        num /= 1024M;
                        str = "??";
                        continue;
                    }
                    num /= 1024M;
                }
            }
            return (num.ToString("N2") + (space ? (" " + str) : str));
        }

        public static string GetMemStringFromBytes(long bytes, int maxLength)
        {
            return GetMemStringFromBytes(bytes, false).PadLeft(maxLength, ' ');
        }

        public static string GetMemStringFromBytes(long bytes, int maxLength, bool space)
        {
            return GetMemStringFromBytes(bytes, space).PadLeft(maxLength, ' ');
        }

        public static string MaxLength(string str, int max)
        {
            if (str.Length > max)
            {
                if (max - 3 > str.Length)
                {
                    return "...";
                }
                else
                {
                    return str.Substring(0, max - 3) + "...";
                }
            }
            else
            {
                return str;
            }
        }

        public static string MaxLengthLeftPadded(string str, int totalWidth, char paddingChar, string appendIfCut)
        {
            if (str.Length > totalWidth)
            {
                return (str.Substring(0, totalWidth - appendIfCut.Length) + appendIfCut);
            }
            return str.PadLeft(totalWidth, paddingChar);
        }

        public static string MaxLengthPadded(string str, int totalWidth, char paddingChar, string appendIfCut)
        {
            if (str.Length > totalWidth)
            {
                return (str.Substring(0, totalWidth - appendIfCut.Length) + appendIfCut);
            }
            return str.PadRight(totalWidth, paddingChar);
        }

        public static void Write(ConsoleColorExt color, string str)
        {
            lock (SyncLock)
            {
                RC.Write(color, str);
            }
        }

        public static void WriteError(string message)
        {
            lock (SyncLock)
            {
                RC.WriteLine(Colors.Error, message);
            }
        }

        public static void WriteException(Exception ex)
        {
            if (ShowFullErrors == true)
            {
                lock (SyncLock)
                {
                    WriteException_Inner(ex);
                }
            }
            else
            {
                WriteError(ex.Message);
            }
        }

        public static void WriteException(string message, Exception ex)
        {
            lock (SyncLock)
            {
                RC.WriteLine(Colors.Error, message);

                if (ShowFullErrors == true)
                {
                    WriteException_Inner(ex);
                }
                else
                {
                    RC.WriteLine(Colors.ErrorDetail, ex.Message);
                }
            }
        }

        public static void WriteInfo(string message)
        {
            lock (SyncLock)
            {
                RC.WriteLine(Colors.Success, message);
            }
        }

        public static void WriteInfo(string message, string detail)
        {
            lock (SyncLock)
            {
                if (ShowFullErrors == true &&
                    String.IsNullOrEmpty(detail) == false)
                {
                    RC.Write(Colors.Success, message);
                    RC.WriteLine(Colors.Normal, " (" + detail + ")");
                }
                else
                {
                    RC.WriteLine(Colors.Success, message);
                }
            }
        }

        /// <summary>
        /// Write a 2 part item line to the console with colors.
        /// </summary>
        /// <param name="color1">Color to write part 1 with.</param>
        /// <param name="str1">String to write.</param>
        /// <param name="color2">Color to write part 2 with.</param>
        /// <param name="str2">String to write.</param>
        public static void WriteItem(ConsoleColorExt color1, string str1, ConsoleColorExt color2, string str2)
        {
            lock (SyncLock)
            {
                WriteItem_Inner(color1, str1, color2, str2);
            }
        }

        public static void WriteItem(ConsoleColorExt color1, string str1, int max, ConsoleColorExt dotColor, ConsoleColorExt color2, string str2)
        {
            lock (SyncLock)
            {
                WriteItem_Inner(color1, str1, max, dotColor, color2, str2);
            }
        }

        public static void WriteLine()
        {
            lock (SyncLock)
            {
                System.Console.WriteLine();
            }
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        public static void WriteLine(ConsoleColorExt color, string str)
        {
            lock (SyncLock)
            {
                RC.WriteLine(color, str);
            }
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        /// <param name="maxLength">The maximum length.</param>
        public static void WriteLine(ConsoleColorExt color, string str, int maxLength)
        {
            lock (SyncLock)
            {
                RC.WriteLine(color, MaxLengthPadded(str, maxLength, ' ', " .."));
            }
        }

        public static void WriteMessage(Direction direction, OscTimeTag? timeTag, string message)
        {
            WriteMessage(direction, timeTag, Colors.Message, message, -1);
        }

        public static void WriteMessage(Direction direction, OscTimeTag? timeTag, ConsoleColorExt messageColor, string message)
        {
            WriteMessage(direction, timeTag, messageColor, message, -1);
        }

        public static void WriteMessage(Direction direction, OscTimeTag? timeTag, string message, int maxLength)
        {
            WriteMessage(direction, timeTag, Colors.Message, message, maxLength);
        }

        public static void WriteMessage(Direction direction, OscTimeTag? timeTag, ConsoleColorExt messageColor, string message, int maxLength)
        {
            int length = 0;
            lock (SyncLock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        RC.Write(Colors.Transmit, "TX ");
                        length += 3;
                        break;

                    case Direction.Receive:
                        RC.Write(Colors.Receive, "RX ");
                        length += 3;
                        break;

                    case Direction.Action:
                        RC.Write(Colors.Action, "   ");
                        length += 3;
                        break;

                    default:
                        break;
                }

                if (timeTag != null)
                {
                    string timeTagString = timeTag.ToString() + " ";
                    RC.Write(Colors.Normal, timeTagString);

                    length += timeTagString.Length;
                }

                RC.WriteLine(messageColor, maxLength > 0 ? MaxLength(message, maxLength - length) : message);
            }
        }

        public static void WriteMessage(Direction direction, ConsoleColorExt prefixColor, string prefix, ConsoleColorExt messageColor, string message)
        {
            WriteMessage(direction, prefixColor, prefix, messageColor, message, -1);
        }

        public static void WriteMessage(Direction direction, ConsoleColorExt prefixColor, string prefix, ConsoleColorExt messageColor, string message, int maxLength)
        {
            int length = 0;
            lock (SyncLock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        RC.Write(Colors.Transmit, "TX ");
                        length += 3;
                        break;

                    case Direction.Receive:
                        RC.Write(Colors.Receive, "RX ");
                        length += 3;
                        break;

                    case Direction.Action:
                        RC.Write(Colors.Action, "   ");
                        length += 3;
                        break;

                    default:
                        break;
                }

                if (string.IsNullOrEmpty(prefix) == false)
                {
                    RC.Write(prefixColor, prefix + " ");

                    length += prefix.Length + 1;
                }

                RC.WriteLine(messageColor, maxLength > 0 ? MaxLengthPadded(message, maxLength - length, ' ', " ..") : message);
            }
        }

        internal static void WriteError(string message, string detail)
        {
            lock (SyncLock)
            {
                if (ShowFullErrors == true &&
                    String.IsNullOrEmpty(detail) == false)
                {
                    RC.Write(Colors.Error, message);
                    RC.WriteLine(Colors.ErrorDetail, " (" + detail + ")");
                }
                else
                {
                    RC.WriteLine(Colors.Error, message);
                }
            }
        }

        /// <summary>
        /// Write a exception to the console.
        /// </summary>
        /// <param name="ex">exception object</param>
        private static void WriteException_Inner(Exception ex)
        {
            RC.WriteLine(Colors.Error, ex.Message);

            WriteStackTrace_Inner(ex.StackTrace);
        }

        /// <summary>
        /// Write a 2 part item line to the console with colors.
        /// </summary>
        /// <param name="color1">Color to write part 1 with.</param>
        /// <param name="str1">String to write.</param>
        /// <param name="color2">Color to write part 2 with.</param>
        /// <param name="str2">String to write.</param>
        private static void WriteItem_Inner(ConsoleColorExt color1, string str1, ConsoleColorExt color2, string str2)
        {
            RC.Write(color1, str1);
            RC.WriteLine(color2, str2);
        }

        private static void WriteItem_Inner(ConsoleColorExt color1, string str1, int max, ConsoleColorExt dotColor, ConsoleColorExt color2, string str2)
        {
            RC.Write(color1, str1);

            if (str1.Length < max)
            {
                RC.Write(dotColor, "".PadLeft(max - str1.Length, '.'));
            }

            RC.Write(color1, ": ");

            RC.WriteLine(color2, str2);
        }

        /// <summary>
        /// Write a stack trace string to the console
        /// </summary>
        /// <param name="trace">trace string</param>
        private static void WriteStackTrace_Inner(string trace)
        {
            if (String.IsNullOrEmpty(trace) == false)
            {
                RC.WriteLine(Colors.ErrorDetail, new string('=', 80));
                RC.WriteLine(Colors.Emphasized, "  " + trace.Replace("\n", "\n  "));
                RC.WriteLine();

                RC.WriteLine(Colors.ErrorDetail, new string('=', 80));
                RC.WriteLine();
            }
        }
    }
}