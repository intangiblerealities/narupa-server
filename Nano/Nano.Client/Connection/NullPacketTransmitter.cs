﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using Rug.Osc;
using System.Collections.Generic;
using System.Net;

namespace Nano.Client
{
    public class NullPacketTransmitter : ITransportPacketTransmitter
    {
        public bool IsConnected => true;

        public void Broadcast(TransportPacketPriority priority, params OscPacket[] packet)
        {
        }

        public void Broadcast(TransportPacketPriority priority, byte[] data, int index, int count)
        {
        }

        public void Flush()
        {
        }

        public void Transmit(TransportPacketPriority priority, IPEndPoint endpoint, params OscPacket[] packet)
        {
        }

        public void Transmit(TransportPacketPriority priority, IEnumerable<IPEndPoint> endpoints, params OscPacket[] packet)
        {
        }

        public void Transmit(TransportPacketPriority priority, byte[] data, int index, int count, IEnumerable<IPEndPoint> endpoints)
        {
        }
    }
}