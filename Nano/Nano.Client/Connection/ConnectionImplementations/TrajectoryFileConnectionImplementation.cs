﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using System;
using System.IO;
using System.Threading;

namespace Nano.Client.ConnectionImplementations
{
    internal class TrajectoryFileConnectionImplementation : IConnectionImplementation
    {
        private readonly SyncBuffer<long> frameStream = new SyncBuffer<long>(7);
        private readonly TrajectoryFileConnectionInfo info;
        private readonly PacketStatistics packetStatistics;
        private readonly IReporter reporter;
        private readonly FileStream trajectoryStream;
        private readonly NullPacketTransmitter transmitter;
        private long firstFramePosition;
        private Thread frameThread;
        private TrajectoryPlayer player;

        public event EventHandler Connected;

        public event EventHandler Disconnected;

        public SimboxConnectionInfo Info => info;

        public ConnectionState State { get; private set; } = ConnectionState.NotConnected;

        public ITransportPacketTransmitter Transmitter => transmitter;

        public TrajectoryFileConnectionImplementation(TrajectoryFileConnectionInfo info, PacketStatistics packetStatistics, IReporter reporter)
        {
            this.info = info;
            this.reporter = reporter;

            transmitter = new NullPacketTransmitter();
            this.packetStatistics = packetStatistics;

            trajectoryStream = new FileStream(info.FilePath, FileMode.Open, FileAccess.Read);

            frameStream.Updated += FrameStream_Updated;
        }

        public void AttachReceiver(ITransportPacketReceiver receiver)
        {
            player = new TrajectoryPlayer(new System.Net.IPEndPoint(info.Address, info.Port), receiver, packetStatistics, trajectoryStream);
            player.ReadThreadExited += Player_ReadThreadExited;
            player.ThreadException += Player_ThreadException;
        }

        public void BeginStart(ClientBase client)
        {
            State = ConnectionState.Connecting;

            firstFramePosition = -1;

            frameStream.Attach(client.IncomingStreams);

            State = ConnectionState.Connected;

            Connected?.Invoke(this, EventArgs.Empty);

            //player.ContinueReading();

            frameThread = new Thread(ReadFrames);

            frameThread.Start();
        }

        public void Dispose()
        {
            player?.Dispose();

            frameThread.Join();

            trajectoryStream?.Dispose();
        }

        private void FrameStream_Updated(ISyncBuffer<long> buffer)
        {
            if (player == null)
            {
                return;
            }

            if (firstFramePosition == -1)
            {
                firstFramePosition = trajectoryStream.Position;
            }

            player.PauseReading();
        }

        private void Player_ReadThreadExited(TrajectoryPlayer player)
        {
            State = ConnectionState.NotConnected;

            Disconnected?.Invoke(player, EventArgs.Empty);
        }

        private void Player_ThreadException(TrajectoryPlayer player, Exception ex)
        {
            reporter.PrintException(ex, "Trajectory player exception");
        }

        private void ReadFrames()
        {
            while (State == ConnectionState.Connected)
            {
                player.ContinueReading();

                frameThread.Join(30);
            }
        }
    }
}