﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Client.ConnectionImplementations;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Rug.Osc;
using System;
using System.IO;

namespace Nano.Client
{
    public enum PlayState
    {
        Play,
        Pause,
        Step,
        Stop,
    }

    public abstract class SimboxConnectionBase : IDisposable
    {
        public readonly ClientBase Client;
        public readonly IConnectionImplementation ConnectionImplementation;
        public readonly SimboxConnectionInfo Info;
        public readonly IReporter Reporter;
        private readonly TrajectoryRecorder recorder;

        protected SimboxConnectionBase(SimboxConnectionInfo info, IReporter reporter, string recordingPath = null)
        {
            Info = info;
            Reporter = reporter;

            PacketStatistics packetStatistics = new PacketStatistics();

            try
            {
                switch (info.ConnectionType)
                {
                    case SimboxConnectionType.Network:
                        ConnectionImplementation = new NetworkConnectionImplementation(info as NetworkConnectionInfo, packetStatistics, reporter);
                        break;

                    case SimboxConnectionType.File:
                        ConnectionImplementation = new TrajectoryFileConnectionImplementation(info as TrajectoryFileConnectionInfo, packetStatistics, reporter);
                        break;

                    case SimboxConnectionType.Cloud:
                        ConnectionImplementation = new CloudConnectionImplemention(info as CloudConnectionInfo, packetStatistics, reporter);
                        break;
                   
                    default:
                        throw new Exception("Unknown connection type.");
                }

                Client = CreateClient(packetStatistics);

                ITransportPacketReceiver receiver;

                if (String.IsNullOrEmpty(recordingPath) == false)
                {
                    FileStream fileStream = new FileStream(Helper.ResolvePath(recordingPath), FileMode.Create, FileAccess.Write);

                    recorder = new TrajectoryRecorder(Client, fileStream);

                    receiver = recorder;
                }
                else
                {
                    receiver = Client;
                }

                ConnectionImplementation.AttachReceiver(receiver);
            }
            catch (Exception ex)
            {
                Reporter.PrintException(ex, "Exception while creating client.");
            }
        }
 
        protected SimboxConnectionBase(IConnectionImplementation connection, IReporter reporter, string recordingPath = null)
        {
            Info = new CustomConnectionInfo("Custom connection");
            
            Reporter = reporter;

            PacketStatistics packetStatistics = new PacketStatistics();

            try
            {
                ConnectionImplementation = connection; 
                
                Client = CreateClient(packetStatistics);

                ITransportPacketReceiver receiver;

                if (String.IsNullOrEmpty(recordingPath) == false)
                {
                    FileStream fileStream = new FileStream(Helper.ResolvePath(recordingPath), FileMode.Create, FileAccess.Write);

                    recorder = new TrajectoryRecorder(Client, fileStream);

                    receiver = recorder;
                }
                else
                {
                    receiver = Client;
                }

                ConnectionImplementation.AttachReceiver(receiver);
            }
            catch (Exception ex)
            {
                Reporter.PrintException(ex, "Exception while creating client.");
            }
        }

        public void Connect()
        {
            ConnectionImplementation.Connected += ConnectionImplementation_Connected;
            ConnectionImplementation.Disconnected += ConnectionImplementation_Disconnected;
            ConnectionImplementation.BeginStart(Client);
        }

        public void Dispose()
        {
            Disconnect();
        }

        public void Send(OscPacket packet)
        {
            Client?.Send(packet);
        }

        protected abstract ClientBase CreateClient(PacketStatistics packetStatistics);

        private void ConnectionImplementation_Connected(object sender, EventArgs e)
        {
            Client.Start();
        }

        private void ConnectionImplementation_Disconnected(object sender, EventArgs e)
        {
            Disconnect();
        }

        private void Disconnect()
        {
            try
            {
                Client?.Dispose();
            }
            catch (Exception ex)
            {
                Reporter.PrintException(ex, "Exception while disposing of client.");
            }

            try
            {
                if (ConnectionImplementation != null)
                {
                    ConnectionImplementation.Connected -= ConnectionImplementation_Connected;
                    ConnectionImplementation.Disconnected -= ConnectionImplementation_Disconnected;

                    ConnectionImplementation?.Dispose();
                }
            }
            catch (Exception ex)
            {
                Reporter.PrintException(ex, "Exception while disposing of connection implementation.");
            }

            try
            {
                recorder?.Dispose();
            }
            catch (Exception ex)
            {
                Reporter.PrintException(ex, "Exception while disposing of trajectory recorder.");
            }
        }
    }
}