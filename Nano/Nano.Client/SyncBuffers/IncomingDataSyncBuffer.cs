﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Transport.Agents;
using Nano.Transport.Streams;
using SlimMath;

namespace Nano.Client
{
    public delegate void SyncBufferEvent<T>(ISyncBuffer<T> buffer);

    public class SyncBuffer<T> : ISyncBuffer<T>
    {
        /// <summary>
        /// ID of the stream.
        /// </summary>
        public readonly ushort ID;

        /// <summary>
        /// Sync lock object.
        /// </summary>
        public readonly object Lock = new object();

        public event SyncBufferEvent<T> Updated;

        /// <summary>
        ///
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        ///
        /// </summary>
        public T[] Data { get; private set; }

        /// <summary>
        /// Gets a flag indicating that the buffer has been updated and needs to be copied.
        /// </summary>
        public bool IsDirty { get; internal set; }

        /// <summary>
        /// Gets the data stream instance.
        /// </summary>
        public DataStream<T> Stream { get; private set; }

        public SyncBuffer(ushort id)
        {
            ID = id;
        }

        public void Attach(IncomingDataAgent agent)
        {
            agent.Added += Inputs_Added;
            agent.Removed += Inputs_Removed;
        }

        public void Detach(IncomingDataAgent agent)
        {
            agent.Added -= Inputs_Added;
            agent.Removed -= Inputs_Removed;

            Count = 0;
            IsDirty = true;
        }

        private void Inputs_Added(IncomingDataAgent agent, ushort streamID, TransportDataStreamReceiver dataStreamReceiver)
        {
            if (streamID != ID)
            {
                return;
            }

            dataStreamReceiver.StreamCreated += OnStreamCreated;
            dataStreamReceiver.StreamDisposed += OnStreamDisposed;
        }

        private void Inputs_Removed(IncomingDataAgent agent, ushort streamID, TransportDataStreamReceiver dataStreamReceiver)
        {
            if (streamID != ID)
            {
                return;
            }

            dataStreamReceiver.StreamCreated -= OnStreamCreated;
            dataStreamReceiver.StreamDisposed -= OnStreamDisposed;
        }

        private void OnStreamCreated(TransportDataStreamReceiver receiver, ITransportDataStream stream)
        {
            lock (Lock)
            {
                Stream = stream as DataStream<T>;
                Data = new T[stream.MaxCount];
            }

            receiver.Updated += OnUpdated;
        }

        private void OnStreamDisposed(TransportDataStreamReceiver receiver, ITransportDataStream stream)
        {
            receiver.Updated -= OnUpdated;

            Stream = null;
        }

        private void OnUpdated(object sender, EventArgs e)
        {
            if (Stream == null)
            {
                return;
            }

            Count = Stream.Count;

            Array.ConstrainedCopy(Stream.Data, 0, Data, 0, Stream.Count);

            IsDirty = true;

            Updated?.Invoke(this);
        }
    }

    public class SyncBuffer_UncompressHalf3ToVector3 : ISyncBuffer<Vector3>
    {
        /// <summary>
        /// ID of the stream.
        /// </summary>
        public readonly ushort ID;

        /// <summary>
        /// Sync lock object.
        /// </summary>
        public readonly object Lock = new object();

        private IDataStream<Half3> compressedDataStream;

        private bool isStreamCompressed;

        private IDataStream<Vector3> uncompressedDataStream;

        public event SyncBufferEvent<Vector3> Updated;

        /// <summary>
        ///
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        ///
        /// </summary>
        public Vector3[] Data { get; private set; }

        /// <summary>
        /// Gets a flag indicating that the buffer has been updated and needs to be copied.
        /// </summary>
        public bool IsDirty { get; set; }

        public SyncBuffer_UncompressHalf3ToVector3(ushort id)
        {
            ID = id;
        }

        public void Attach(IncomingDataAgent agent)
        {
            agent.Added += Inputs_Added;
            agent.Removed += Inputs_Removed;
        }

        public void Detach(IncomingDataAgent agent)
        {
            agent.Added -= Inputs_Added;
            agent.Removed -= Inputs_Removed;

            Count = 0;
            IsDirty = true;
        }

        private void Inputs_Added(IncomingDataAgent agent, ushort streamID, TransportDataStreamReceiver dataStreamReceiver)
        {
            if (streamID == ID)
            {
                dataStreamReceiver.StreamCreated += OnStreamCreated;
                dataStreamReceiver.StreamDisposed += OnStreamDisposed;
            }
        }

        private void Inputs_Removed(IncomingDataAgent agent, ushort streamID, TransportDataStreamReceiver dataStreamReceiver)
        {
            if (streamID == ID)
            {
                dataStreamReceiver.StreamCreated -= OnStreamCreated;
                dataStreamReceiver.StreamDisposed -= OnStreamDisposed;
            }
        }

        private void OnStreamCreated(TransportDataStreamReceiver receiver, ITransportDataStream stream)
        {
            lock (Lock)
            {
                switch (stream)
                {
                    case IDataStream<Vector3> dataStream:
                        uncompressedDataStream = dataStream;
                        isStreamCompressed = false;
                        break;

                    case IDataStream<Half3> dataStream:
                        compressedDataStream = dataStream;
                        isStreamCompressed = true;
                        break;

                    default:
                        throw new SimboxException(SimboxExceptionType.TypesAreIncompatible, $"Stream {stream.Format} is not a Vector3 or Half3 stream.");
                }

                Data = new Vector3[stream.MaxCount];
            }

            receiver.Updated += OnUpdated;
        }

        private void OnStreamDisposed(TransportDataStreamReceiver receiver, ITransportDataStream stream)
        {
            receiver.Updated -= OnUpdated;

            uncompressedDataStream = null;
            compressedDataStream = null;
        }

        private void OnUpdated(object sender, EventArgs e)
        {
            if (isStreamCompressed == false)
            {
                if (uncompressedDataStream == null)
                {
                    return;
                }

                Count = uncompressedDataStream.Count;

                Array.ConstrainedCopy(uncompressedDataStream.Data, 0, Data, 0, uncompressedDataStream.Count);
            }
            else
            {
                if (compressedDataStream == null)
                {
                    return;
                }

                Count = compressedDataStream.Count;

                Half3[] source = compressedDataStream.Data;
                Vector3[] destination = Data;

                for (int i = 0; i < source.Length; i++)
                {
                    Vector3 v3 = source[i];

                    destination[i] = new Vector3(v3.X, v3.Y, v3.Z);
                }
            }

            IsDirty = true;

            Updated?.Invoke(this);
        }
    }
}