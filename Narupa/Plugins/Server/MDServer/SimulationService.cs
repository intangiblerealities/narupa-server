﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Server;
using Nano.Server.Basic;
using Nano.Transport.Comms;
using Rug.Cmd;

namespace MDServer
{
    [XmlName("SimulationService")]
    public class SimulationService : IService
    {
        private StringArgument simulationPath;
        private bool simulationFileOverride;
        public Uri SimulationFile { get; set; } 

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            string simulationFileString = Helper.GetAttributeValue(node, nameof(SimulationFile), null);

            if (simulationFileOverride == false && simulationFileString != null)
            {
                SimulationFile = new Uri(simulationFileString, UriKind.RelativeOrAbsolute);                 
            }

            Endpoint = Loader.LoadObject<IServiceEndpoint>(context, nameof(Endpoint), node)?? new ServiceEndpoint(); 
            StreamOptions = Loader.LoadObject<StreamOptions>(context, nameof(StreamOptions), node) ?? new StreamOptions(); 
        }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            if (SimulationFile != null)
            {
                Helper.AppendAttributeAndValue(element, nameof(SimulationFile), SimulationFile.ToString());
            }
            
            Loader.SaveObject(context, element, Endpoint, nameof(Endpoint)); 
            Loader.SaveObject(context, element, StreamOptions, nameof(StreamOptions));
            
            return element;
        }

        /// <inheritdoc />
        public void RegisterArguments(ArgumentParser parser)
        {
            (Endpoint as IServerComponent)?.RegisterArguments(parser); 
            simulationPath = new StringArgument("Simulation Path", "Path to simulation file", "Path to simulation file");
            parser.Add("-", "S", "Simulation", simulationPath);

        }

        /// <inheritdoc />
        public void ValidateArguments(IReporter reporter)
        {
            (Endpoint as IServerComponent)?.ValidateArguments(reporter); 
            
            if (simulationPath.Defined == true)
            {
                SimulationFile = new Uri(simulationPath.Value, UriKind.RelativeOrAbsolute);
                simulationFileOverride = true;
            }
        }

        /// <inheritdoc />
        public IServiceEndpoint Endpoint { get; set; }

        public StreamOptions StreamOptions { get; private set; } 

        /// <inheritdoc />
        public async Task Start(IServer serverContext, CancellationToken cancel)
        {
            await Task.Yield(); 
            
            IReporter reporter = serverContext.ReportManager.BeginReport(ReportContext.Service, "service", out Uri logIdentifier);
            
            DefaultOscMessageFilterMode filterMode; 
                
            switch (reporter.ReportVerbosity)
            {
                case ReportVerbosity.Debug:
                    filterMode = DefaultOscMessageFilterMode.All; 
                    break;
                case ReportVerbosity.Detail:
                    filterMode = DefaultOscMessageFilterMode.All;
                    break;
                case ReportVerbosity.Normal:
                    filterMode = DefaultOscMessageFilterMode.AhoyOnly;
                    break;
                case ReportVerbosity.Emphasized:
                    filterMode = DefaultOscMessageFilterMode.None;
                    break;
                case ReportVerbosity.ExceptionsOnly:
                    filterMode = DefaultOscMessageFilterMode.None;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            reporter.OscMessageFilter = new DefaultOscMessageFilter {FilterMode = filterMode}; 
            
            MDServer.SimulationServerImplementation server = null;
            
            try
            {
                server = new MDServer.SimulationServerImplementation(
                    serverContext,
                    serverContext.Name,
                    StreamOptions,
                    Endpoint.EndpointUri.Scheme == "ws" || Endpoint.EndpointUri.Scheme == "http" || Endpoint.EndpointUri.Scheme == "https" 
                        ? ConnectionType.WebSocket 
                        : ConnectionType.Tcp,
                    Endpoint.EndpointUri.Port,
                    serverContext.SecurityProvider.TransportSecurityProvider, 
                    reporter);

                if (SimulationFile != null)
                {
                    await server.EnqueueLoad(SimulationFile, false); 
                }

                await server.StartAsync(cancel); 
            }
            catch (TaskCanceledException  )
            {
                reporter.PrintDebug("Simulation service task canceled"); 
            }
            catch (Exception ex)
            {
                reporter.PrintException(ex, "Exception in simulation service task");
            }
            finally
            {
                server?.Dispose(); 
                
                reporter.PrintEmphasized("Simulation service closed");

                serverContext.ReportManager.EndReport(logIdentifier);
            }
        }
    }
}