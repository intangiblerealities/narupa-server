﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;

namespace Narupa.Trajectory.Load.PDB
{
    /// <summary>
    ///     Initialises the PdbChain class.
    /// </summary>
    public class PdbChain
    {
        /// <summary>
        ///     Letter identifier of the PDB chain.
        /// </summary>
        public string Letter;

        /// <summary>
        ///     Creates a list of residues within the chain.
        /// </summary>
        public List<PdbResidue> Residues = new List<PdbResidue>();

        /// <summary>
        ///     Initialises the properties of a chain.
        /// </summary>
        /// <param name="newChainChar"></param>
        public PdbChain(string newChainChar)
        {
            if (newChainChar == "" || string.IsNullOrEmpty(newChainChar) || string.IsNullOrWhiteSpace(newChainChar))
                Letter = "A";
            else Letter = newChainChar;
        }
    }
}