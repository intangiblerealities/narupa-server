﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Narupa.Trajectory.Load.PDB
{
    /// <summary>
    ///     Initialises the PdbBond class.
    /// </summary>
    public class PdbBond : IEquatable<PdbBond>
    {
        /// <summary>
        ///     The first atom in the bond.
        /// </summary>
        public readonly PdbAtom From;

        /// <summary>
        ///     The second atom in the bond.
        /// </summary>
        public readonly PdbAtom To;

        /// <summary>
        ///     Initialises the properties of a bond.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public PdbBond(PdbAtom from, PdbAtom to)
        {
            if (from.SerialNumber > to.SerialNumber)
            {
                To = from;
                From = to;
            }
            else
            {
                From = from;
                To = to;
            }
        }

        /// <inheritdoc />
        public bool Equals(PdbBond other)
        {
            if (other == null) return false;
            if (From.Equals(other.From) && To.Equals(other.To)) return true;
            if (From.Equals(To) && To.Equals(other.From)) return true;
            return false;
        }

        /// <inheritdoc />
        public override bool Equals(object other)
        {
            if (other is PdbBond)
                return Equals((PdbBond) other);

            return false;
        }

        /// <summary>
        ///     Uses Cantor pairing function to generate hash code for bond.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            var a = From.SerialNumber;
            var b = To.SerialNumber;
            return (a + b) * (a + b + 1) / 2 + a;
        }
    }
}