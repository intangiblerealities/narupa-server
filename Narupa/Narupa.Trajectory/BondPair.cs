// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Runtime.InteropServices;

namespace Narupa.Trajectory
{
    /// <summary>
    ///     A tuple struct representing a bond.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BondPair : IEquatable<BondPair>
    {
        /// <summary>
        ///     First index in the bond.
        /// </summary>
        public int A;

        /// <summary>
        ///     Second index in the bond.
        /// </summary>
        public int B;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BondPair" /> struct.
        /// </summary>
        /// <param name="a"> Index of atom a</param>
        /// <param name="b"> Index of atom b.</param>
        public BondPair(int a, int b) : this()
        {
            if (a < b)
            {
                A = a;
                B = b;
            }
            else
            {
                A = b;
                B = a;
            }
        }
        
        /// <summary>
        ///     Initializes a new instance of the <see cref="BondPair" /> struct.
        /// </summary>
        /// <param name="a"> Index of atom a</param>
        /// <param name="b"> Index of atom b.</param>
        public BondPair(ushort a, ushort b) : this()
        {
            if (a < b)
            {
                A = a;
                B = b;
            }
            else
            {
                A = b;
                B = a;
            }
        }

        /// <summary>
        ///     Gets the source.
        /// </summary>
        /// <value>The source.</value>
        public int Source => A;

        /// <summary>
        ///     Gets the target.
        /// </summary>
        /// <value>The target.</value>
        public int Target => B;

        /// <summary>
        ///     Determines whether the specified <see cref="BondPair" /> is equal to this instance.
        /// </summary>
        /// <param name="other">The <see cref="BondPair" /> to compare with this instance.</param>
        /// <returns><c>true</c> if the specified <see cref="BondPair" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        /// >
        public bool Equals(BondPair other)
        {
            return A == other.A && B == other.B;
        }

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            switch (obj)
            {
                case BondPair bondPair:
                    return Equals(bondPair);

                default:
                    return false;
            }
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return (A + B) * (A + B + 1) / 2 + A;
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return $"{A} -> {B}";
        }
    }
}