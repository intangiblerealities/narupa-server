// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Nano.Science;

namespace Narupa.Trajectory
{
    /// <summary>
    ///     Represents a typical molecular dynamics topology, using a PDB structure of chains, residues and atoms.
    /// </summary>
    /// <remarks>
    ///     Inspired by the MDTraj topology, and provides a subset of its functionality.
    /// </remarks>
    public class Topology : ITopology
    {
        /// <summary>
        /// Number of residues in the topology.
        /// </summary>
        public int NumberOfResidues => residues.Count;
        /// <summary>
        /// Number of chains in the topology
        /// </summary>
        public int NumberOfChains => chains.Count;
        
        private readonly List<Atom> atoms = new List<Atom>();
        private readonly HashSet<BondPair> bonds = new HashSet<BondPair>();

        private readonly List<Chain> chains = new List<Chain>();
        private readonly List<Element> elements = new List<Element>();
        private readonly List<Residue> residues = new List<Residue>();

        /// <summary>
        /// Returns an enumerator over the chains of the topology.
        /// </summary>
        public IEnumerable<Chain> Chains
        {
            get
            {
                foreach (var chain in chains) yield return chain;
            }
        }

        /// <summary>
        /// Returns an enumerator over the residues of the topology.
        /// </summary>
        public IEnumerable<Residue> Residues
        {
            get
            {
                foreach (var residue in residues) yield return residue;
            }
        }

        /// <summary>
        /// Returns an enumerator over the atoms of the topology.
        /// </summary>
        public IEnumerable<Atom> Atoms
        {
            get
            {
                foreach (var atom in atoms) yield return atom;
            }
        }

        
        /// <inheritdoc />
        public IEnumerable<BondPair> Bonds => bonds;
        /// <inheritdoc />
        public IList<Element> Elements => elements;
        /// <inheritdoc />
        public int NumberOfAtoms => atoms.Count;

        /// <summary>
        ///     Adds a bond to the topology between atoms indexed by A and B.
        /// </summary>
        /// <param name="a">Index of atom A.</param>
        /// <param name="b">Index of atom B.</param>
        /// <returns></returns>
        public BondPair AddBond(int a, int b)
        {
            if (a > NumberOfAtoms || b > NumberOfAtoms)
                throw new Exception($"Attempted to add bond to atom that is out of range, between atoms {a} and {b}");
            var bond = new BondPair(a, b);
            bonds.Add(bond);
            return bond;
        }

        /// <summary>
        ///     Adds an atom to the topology.
        /// </summary>
        /// <param name="name">Name of the atom.</param>
        /// <param name="element">Element to associate with the atom.</param>
        /// <param name="residue">Residue the atom belongs to.</param>
        /// <param name="serial">The serial of the atom, if appropriate.</param>
        /// <returns>
        ///     <see cref="Atom" />
        /// </returns>
        public Atom AddAtom(string name, Element element, Residue residue, int serial = -1)
        {
            var index = atoms.Count + 1;
            if (serial == -1)
                serial = index;
            var atom = new Atom(index, name, element, residue, serial);

            atoms.Add(atom);
            residue.AddAtom(atom);
            elements.Add(element);
            return atom;
        }

        /// <summary>
        ///     Adds a bond to the topology between atoms A and B.
        /// </summary>
        /// <param name="a">Atom A.</param>
        /// <param name="b">Atom B.</param>
        /// <returns></returns>
        public BondPair AddBond(Atom a, Atom b)
        {
            var bond = new BondPair(a.Index, b.Index);
            bonds.Add(bond);
            return bond;
        }

        /// <summary>
        /// Adds a bond to the topology.
        /// </summary>
        /// <param name="bond">Bond.</param>
        public void AddBond(BondPair bond)
        {
            AddBond(bond.A, bond.B);
        }

        /// <summary>
        ///     Adds an empty chain to the topology.
        /// </summary>
        /// <returns>
        ///     <see cref="Chain" />
        /// </returns>
        public Chain AddChain()
        {
            var chain = new Chain();
            chains.Add(new Chain());
            return chain;
        }

        /// <summary>
        ///     Adds a residue to the topology
        /// </summary>
        /// <param name="name">Name of the residue.</param>
        /// <param name="chain">Chain to add residue to.</param>
        /// <param name="resID">The residue ID of the residue.</param>
        /// <param name="segmentID">The segment ID of the residue.</param>
        /// <returns>
        ///     <see cref="Residue" />
        /// </returns>
        public Residue AddResidue(string name, Chain chain, int resID = -1, string segmentID = "")
        {
            if (resID == -1)
                resID = NumberOfResidues + 1;
            var res = new Residue(name, chain, resID, segmentID);
            residues.Add(res);
            chain.AddResidue(res);
            return res;
        }

        /// <summary>
        ///     Returns the atom at the specified index.
        /// </summary>
        /// <param name="index">Index of the atom.</param>
        /// <returns>
        ///     <see cref="Atom" />
        /// </returns>
        public Atom GetAtom(int index)
        {
            return atoms[index];
        }

        /// <summary>
        ///     Returns the residue at the specified index.
        /// </summary>
        /// <param name="index">Index of the residue.</param>
        /// <returns>
        ///     <see cref="Residue" />
        /// </returns>
        public Residue GetResidue(int index)
        {
            return residues[index];
        }

        /// <summary>
        ///     Returns the chain at the specified index.
        /// </summary>
        /// <param name="index">Index of the chain.</param>
        /// <returns>
        ///     <see cref="Chain" />
        /// </returns>
        public Chain GetChain(int index)
        {
            return chains[index];
        }

        /// <summary>
        ///     Produces a copy of this trajectory.
        /// </summary>
        /// <returns>
        ///     <see cref="Topology" />
        /// </returns>
        public Topology Copy()
        {
            var top = new Topology();
            foreach (var chain in chains)
            {
                var newChain = top.AddChain();
                foreach (var residue in chain.Residues)
                {
                    var newResidue = top.AddResidue(residue.Name, newChain, residue.ResId, residue.SegmentId);
                    foreach (var atom in residue.Atoms) top.AddAtom(atom.Name, atom.Element, newResidue, atom.Serial);
                }
            }

            foreach (var bond in bonds) top.AddBond(bond.A, bond.B);

            return top;
        }
    }
}