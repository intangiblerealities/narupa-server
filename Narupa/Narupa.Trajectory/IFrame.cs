// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using SlimMath;

namespace Narupa.Trajectory
{
    /// <summary>
    ///     Represents the frame of a trajectory.
    /// </summary>
    public interface IFrame
    {
        /// <summary>
        ///     Topology of the system.
        /// </summary>
        /// <remarks>
        ///     For most implementations of a <see cref="ITrajectory" />, the topology is the same between frames. However,
        ///     in reactive trajectories it may change.
        /// </remarks>
        ITopology Topology { get; }

        /// <summary>
        ///     The positions of the particles in the system, in nanometers.
        /// </summary>
        IList<Vector3> Positions { get; }
    }
}