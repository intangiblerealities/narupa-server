#!/usr/bin/env bash
cd DocumentationSource
mkdir -p ../Documentation
doxygen
cp -r resources ../Documentation/html/
cp documentation.html ../Documentation
cd ..

# Script to edit the pages visible on top bar.
# cat DocumentationSource/narupa-menudata.js > Documentation/html/menudata.js
